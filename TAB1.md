# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the VMwarevRealizeOrchestrator System. The API that was used to build the adapter for VMwarevRealizeOrchestrator is usually available in the report directory of this adapter. The adapter utilizes the VMwarevRealizeOrchestrator API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The VMware vRealize Orchestrator adapter from Itential is used to integrate the Itential Automation Platform (IAP) with VMware vRealize Orchestrator. 

With this adapter you have the ability to perform operations with VMware vRealize Orchestrator such as:
- Inventory
- Policy
- Workflows

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
