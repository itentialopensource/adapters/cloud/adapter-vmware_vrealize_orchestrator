# VMware vRealize Orchestrator

Vendor: VMware
Homepage: https://www.vmware.com/

Product: vRealize Orchestrator
Product Page: https://docs.vmware.com/en/vRealize-Orchestrator/index.html
## Introduction
We classify VMware vRealize Orchestrator into the Cloud domain as VMware vRealize Orchestrator provides automation and orchestration capabilities essential for cloud environments. It enables the automation of workflows, integration with VMware's products and dynamic resource management.

"VMware vRealize Orchestrator is a development- and process-automation platform that provides a library of extensible workflows to allow you to create and run automated, configurable processes to manage VMware products as well as other third-party technologies"

## Why Integrate
The VMware vRealize Orchestrator adapter from Itential is used to integrate the Itential Automation Platform (IAP) with VMware vRealize Orchestrator. 

With this adapter you have the ability to perform operations with VMware vRealize Orchestrator such as:
- Inventory
- Policy
- Workflows

## Additional Product Documentation
The [API documents for VMware vRealize Orchestrator](https://developer.broadcom.com/xapis/vrealize-orchestrator-api/latest/)
