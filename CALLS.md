## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for VMware vRealize Orchestrator. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for VMware vRealize Orchestrator.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the VMwarevRealizeOrchestrator. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionsForTask(id, callback)</td>
    <td style="padding:15px">Get task permissions</td>
    <td style="padding:15px">{base_path}/{version}/tasks/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertPermissionsForTask(id, body, callback)</td>
    <td style="padding:15px">Set task permissions</td>
    <td style="padding:15px">{base_path}/{version}/tasks/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionsForTask(id, callback)</td>
    <td style="padding:15px">Delete task permissions</td>
    <td style="padding:15px">{base_path}/{version}/tasks/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTask(id, callback)</td>
    <td style="padding:15px">Get task</td>
    <td style="padding:15px">{base_path}/{version}/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTask(id, body, callback)</td>
    <td style="padding:15px">Update task</td>
    <td style="padding:15px">{base_path}/{version}/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTask(id, callback)</td>
    <td style="padding:15px">Delete task</td>
    <td style="padding:15px">{base_path}/{version}/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTasks(callback)</td>
    <td style="padding:15px">Get tasks</td>
    <td style="padding:15px">{base_path}/{version}/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTask(body, callback)</td>
    <td style="padding:15px">Create task</td>
    <td style="padding:15px">{base_path}/{version}/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExecutions(id, callback)</td>
    <td style="padding:15px">Get task executions</td>
    <td style="padding:15px">{base_path}/{version}/tasks/{pathv1}/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionRuleByRuleIdForTask(id, ruleId, callback)</td>
    <td style="padding:15px">Get permission rule</td>
    <td style="padding:15px">{base_path}/{version}/tasks/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePermissionRuleByRuleIdForTask(id, ruleId, body, callback)</td>
    <td style="padding:15px">Update permission rule</td>
    <td style="padding:15px">{base_path}/{version}/tasks/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionRuleByRuleIdForTask(id, ruleId, callback)</td>
    <td style="padding:15px">Delete permission rule</td>
    <td style="padding:15px">{base_path}/{version}/tasks/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allInteractionPresentations(workflowId, executionId, callback)</td>
    <td style="padding:15px">Get all interaction presentations</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/interaction/presentation/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startInteractionPresentation(workflowId, executionId, body, callback)</td>
    <td style="padding:15px">Start interaction presentation</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/interaction/presentation/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPresentationForInteraction(workflowId, executionId, callback)</td>
    <td style="padding:15px">Get presentation for interaction</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/interaction/presentation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadExecutionByPresentationExecutionId(workflowId, executionId, presentationExecutionId, callback)</td>
    <td style="padding:15px">Load execution</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/interaction/presentation/instances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePresentationByPresentationExecutionId(workflowId, executionId, presentationExecutionId, body, callback)</td>
    <td style="padding:15px">Update presentation</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/interaction/presentation/instances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePresentationPostByPresentationExecutionId(workflowId, executionId, presentationExecutionId, body, callback)</td>
    <td style="padding:15px">Update presentation</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/interaction/presentation/instances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflowExecutionPresentationInstance(workflowId, executionId, presentationExecutionId, callback)</td>
    <td style="padding:15px">Delete workflow execution</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/interaction/presentation/instances/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTagOwners(callback)</td>
    <td style="padding:15px">List tag owners</td>
    <td style="padding:15px">{base_path}/{version}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagsByTagNameAndOwner(owner, tagName, callback)</td>
    <td style="padding:15px">Get tags by tag name and owner</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTagsByOwner(owner, callback)</td>
    <td style="padding:15px">List tags by owner</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">untagObjectsByTagNameAndOwner(owner, callback)</td>
    <td style="padding:15px">Untag object</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllTags(callback)</td>
    <td style="padding:15px">List global and self tags</td>
    <td style="padding:15px">{base_path}/{version}/tags/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aboutInfo(callback)</td>
    <td style="padding:15px">Get about info</td>
    <td style="padding:15px">{base_path}/{version}/about?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getXmlSchemaByName(name, callback)</td>
    <td style="padding:15px">Get REST XSD schema file</td>
    <td style="padding:15px">{base_path}/{version}/schema/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enumerateServices(callback)</td>
    <td style="padding:15px">Enumerate services</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">docs(callback)</td>
    <td style="padding:15px">Redirect docs to docs/index.html</td>
    <td style="padding:15px">{base_path}/{version}/docs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">healthStatus(components, callback)</td>
    <td style="padding:15px">Get health status</td>
    <td style="padding:15px">{base_path}/{version}/healthstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getXmlSchema(callback)</td>
    <td style="padding:15px">Get REST XSD schema file</td>
    <td style="padding:15px">{base_path}/{version}/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">status(callback)</td>
    <td style="padding:15px">Get vRA registration state</td>
    <td style="padding:15px">{base_path}/{version}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">supportedApiVersions(callback)</td>
    <td style="padding:15px">List supported API versions</td>
    <td style="padding:15px">{base_path}/{version}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadExecutionByExecutionId(workflowId, executionId, callback)</td>
    <td style="padding:15px">Load Execution</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/presentation/instances/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePresentationByExecutionId(workflowId, executionId, body, callback)</td>
    <td style="padding:15px">Update presentation</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/presentation/instances/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePresentationPostByExecutionId(workflowId, executionId, body, callback)</td>
    <td style="padding:15px">Update presentation</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/presentation/instances/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflowExecutionInstance(executionId, workflowId, callback)</td>
    <td style="padding:15px">Delete workflow execution</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/presentation/instances/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPresentationFor(workflowId, callback)</td>
    <td style="padding:15px">Get presentation</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/presentation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allPresentation(workflowId, callback)</td>
    <td style="padding:15px">Get all presentations</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/presentation/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startPresentation(workflowId, body, callback)</td>
    <td style="padding:15px">Start presentation</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/presentation/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadWorkflowSchema(id, callback)</td>
    <td style="padding:15px">Access workflow's schema image</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflow(id, callback)</td>
    <td style="padding:15px">Retrieves the definition of a workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionsForWorkflow(id, callback)</td>
    <td style="padding:15px">Retrieve workflow permissions</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertPermissionsForWorkflow(id, body, callback)</td>
    <td style="padding:15px">Set workflow permissions</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionsForWorkflow(id, callback)</td>
    <td style="padding:15px">Deletes all permissions</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUserInteractionsForWorkflow(workflowId, callback)</td>
    <td style="padding:15px">Retrieves all user interactions for a requested workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/interactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadWorkflowIcon(id, callback)</td>
    <td style="padding:15px">Access workflow's icon</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/icon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validate(body, callback)</td>
    <td style="padding:15px">Validate workflow content</td>
    <td style="padding:15px">{base_path}/{version}/workflows/validate/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTasksForWorkflow(workflowId, callback)</td>
    <td style="padding:15px">Retrieves all scheduled tasks for a requested workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllWorkflows(maxResult, startIndex, queryCount, keys, conditions, sortOrders, tags, callback)</td>
    <td style="padding:15px">Get all workflows</td>
    <td style="padding:15px">{base_path}/{version}/workflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importWorkflow(categoryId, overwrite, bodyFormData, callback)</td>
    <td style="padding:15px">Import/upload a workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateExisting(workflowId, callback)</td>
    <td style="padding:15px">Validate existing workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflowByWorkflowId(workflowId, force, forceDeleteLocked, callback)</td>
    <td style="padding:15px">Delete a workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadWorkflowSchemaContent(id, callback)</td>
    <td style="padding:15px">Access workflow's schema</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionRuleByRuleIdForWorkflow(id, ruleId, callback)</td>
    <td style="padding:15px">Retrieves permission rule</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePermissionRuleByRuleIdForWorkflow(id, ruleId, body, callback)</td>
    <td style="padding:15px">Updates permission rule</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionRuleByRuleIdForWorkflow(id, ruleId, callback)</td>
    <td style="padding:15px">Delete permission rule</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findById(namespace, type, id, keys, callback)</td>
    <td style="padding:15px">Find by id</td>
    <td style="padding:15px">{base_path}/{version}/catalog/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listObjectTags(namespace, type, id, searchScope, callback)</td>
    <td style="padding:15px">List object tags</td>
    <td style="padding:15px">{base_path}/{version}/catalog/{pathv1}/{pathv2}/{pathv3}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tagObject(namespace, type, id, body, callback)</td>
    <td style="padding:15px">Attach tag to entity</td>
    <td style="padding:15px">{base_path}/{version}/catalog/{pathv1}/{pathv2}/{pathv3}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchPluginMetadata(namespace, showHiddenTypes, callback)</td>
    <td style="padding:15px">Fetch plugin metadata</td>
    <td style="padding:15px">{base_path}/{version}/catalog/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findSimpleListQuery(namespace, type, maxResult, startIndex, startFromId, queryCount, keys, conditions, sortOrders, rootObject, tags, callback)</td>
    <td style="padding:15px">List objects for specific type</td>
    <td style="padding:15px">{base_path}/{version}/catalog/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findRootElement(namespace, keys, callback)</td>
    <td style="padding:15px">Find root element</td>
    <td style="padding:15px">{base_path}/{version}/catalog/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findByRelation(namespace, parentType, parentId, relationName, maxResult, startIndex, queryCount, keys, conditions, sortOrders, callback)</td>
    <td style="padding:15px">Find by relation</td>
    <td style="padding:15px">{base_path}/{version}/catalog/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNamespaces(callback)</td>
    <td style="padding:15px">List namespaces</td>
    <td style="padding:15px">{base_path}/{version}/catalog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadIconForModule(namespace, callback)</td>
    <td style="padding:15px">Download icon for module</td>
    <td style="padding:15px">{base_path}/{version}/catalog/{pathv1}/metadata/icon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">untagObject(namespace, type, id, tagname, callback)</td>
    <td style="padding:15px">Remove tag from entity</td>
    <td style="padding:15px">{base_path}/{version}/catalog/{pathv1}/{pathv2}/{pathv3}/tags/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadIconForType(namespace, type, callback)</td>
    <td style="padding:15px">Download icon for type</td>
    <td style="padding:15px">{base_path}/{version}/catalog/{pathv1}/{pathv2}/metadata/icon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importActionByCategoryName(categoryName, bodyFormData, callback)</td>
    <td style="padding:15px">Import action</td>
    <td style="padding:15px">{base_path}/{version}/content/actions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listActions(maxResult, startIndex, queryCount, keys, conditions, sortOrders, tags, callback)</td>
    <td style="padding:15px">List actions</td>
    <td style="padding:15px">{base_path}/{version}/content/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listWorkflows(maxResult, startIndex, queryCount, keys, conditions, sortOrders, tags, callback)</td>
    <td style="padding:15px">List workflows</td>
    <td style="padding:15px">{base_path}/{version}/content/workflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportActionByActionName(actionName, callback)</td>
    <td style="padding:15px">Export action</td>
    <td style="padding:15px">{base_path}/{version}/content/actions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteActionByActionName(actionName, force, callback)</td>
    <td style="padding:15px">Delete action</td>
    <td style="padding:15px">{base_path}/{version}/content/actions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listContentTypes(callback)</td>
    <td style="padding:15px">List content types</td>
    <td style="padding:15px">{base_path}/{version}/content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportPackage(packageName, exportConfigurationAttributeValues, exportGlobalTags, exportVersionHistory, exportConfigSecureStringAttributeValues, callback)</td>
    <td style="padding:15px">Export package</td>
    <td style="padding:15px">{base_path}/{version}/content/packages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePackageWithContent(packageName, force, callback)</td>
    <td style="padding:15px">Delete package</td>
    <td style="padding:15px">{base_path}/{version}/content/packages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllPackagesByContent(callback)</td>
    <td style="padding:15px">List all packages</td>
    <td style="padding:15px">{base_path}/{version}/content/packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importPackageByContent(overwrite, importConfigurationAttributeValues, tagImportMode = 'DoNotImport', importConfigSecureStringAttributeValues, bodyFormData, callback)</td>
    <td style="padding:15px">Import package</td>
    <td style="padding:15px">{base_path}/{version}/content/packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importWorkflowByCategoryId(categoryId, bodyFormData, callback)</td>
    <td style="padding:15px">Import workflow</td>
    <td style="padding:15px">{base_path}/{version}/content/workflows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportWorkflow(workflowId, callback)</td>
    <td style="padding:15px">Export workflow</td>
    <td style="padding:15px">{base_path}/{version}/content/workflows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflowByWorkflowIdInContent(workflowId, force, callback)</td>
    <td style="padding:15px">Delete workflow</td>
    <td style="padding:15px">{base_path}/{version}/content/workflows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowExecutionState(workflowId, executionId, callback)</td>
    <td style="padding:15px">Get workflow execution state</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelWorkflowExecution(workflowId, executionId, callback)</td>
    <td style="padding:15px">Cancel workflow run</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllExecutions(workflowId, maxResult, startIndex, keys, conditions, sortOrders, callback)</td>
    <td style="padding:15px">Get all executions</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startWorkflowExecution(workflowId, body, callback)</td>
    <td style="padding:15px">Start workflow execution</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserInteraction(workflowId, executionId, callback)</td>
    <td style="padding:15px">Get user interaction</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/interaction?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">answerUserInteraction(workflowId, executionId, body, callback)</td>
    <td style="padding:15px">Answers user interaction</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/interaction?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowExecution(workflowId, executionId, showDetails, expand, callback)</td>
    <td style="padding:15px">Get workflow execution</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflowExecution(workflowId, executionId, callback)</td>
    <td style="padding:15px">Delete workflow run</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowExecutionLogs(workflowId, executionId, maxResult, conditions, callback)</td>
    <td style="padding:15px">Get workflow run logs</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowExecutionStatistics(workflowId, executionId, callback)</td>
    <td style="padding:15px">Get workflow execution statistics</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowExecutionSyslogs(workflowId, executionId, maxResult, conditions, callback)</td>
    <td style="padding:15px">Get workflow run system and event logs</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/executions/{pathv2}/syslogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">packageDetails(packageName, callback)</td>
    <td style="padding:15px">Package details</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPackage(packageName, body, callback)</td>
    <td style="padding:15px">Create new package</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePackage(packageName, option, callback)</td>
    <td style="padding:15px">Delete package</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePackage(packageName, body, callback)</td>
    <td style="padding:15px">Update package</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResourceElement(packageName, id, callback)</td>
    <td style="padding:15px">Delete resource element</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/resource/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImportPackageDetails(bodyFormData, callback)</td>
    <td style="padding:15px">Analyses uploaded package file and returns details about package elements and certificate</td>
    <td style="padding:15px">{base_path}/{version}/packages/import-details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addConfigurationElementCategoryToPackage(packageName, configurationCategoryName, callback)</td>
    <td style="padding:15px">Add configuration category to package</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/configuration_category/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllPackages(callback)</td>
    <td style="padding:15px">List all packages</td>
    <td style="padding:15px">{base_path}/{version}/packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importPackage(overwrite, importConfigurationAttributeValues, tagImportMode = 'DoNotImport', importConfigSecureStringAttributeValues, bodyFormData, callback)</td>
    <td style="padding:15px">Import package</td>
    <td style="padding:15px">{base_path}/{version}/packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionRuleByRuleIdForPackage(packageName, ruleId, callback)</td>
    <td style="padding:15px">Get permission rule</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePermissionRuleByRuleIdForPackage(packageName, ruleId, body, callback)</td>
    <td style="padding:15px">Update permission rule</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionRuleByRuleIdForPackage(packageName, ruleId, callback)</td>
    <td style="padding:15px">Delete permission rule</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigurationElement(packageName, id, callback)</td>
    <td style="padding:15px">Delete configuration element</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/configuration/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRecourceElementToPackage(packageName, resourceId, callback)</td>
    <td style="padding:15px">Add recource element to package</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/resource/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addWorkflowToPackage(packageName, workflowId, callback)</td>
    <td style="padding:15px">Add workflow to package</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/workflow/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addConfigurationElementToPackage(packageName, configurationId, callback)</td>
    <td style="padding:15px">Add configuration element to package</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/configuration/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionsForObjectPackage(packageName, callback)</td>
    <td style="padding:15px">Get package permissions</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertPermissionsForPackage(packageName, body, callback)</td>
    <td style="padding:15px">Set permissions for package</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionsForPackage(packageName, callback)</td>
    <td style="padding:15px">Deletes all permissions</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addActionsToPackage(packageName, categoryName, callback)</td>
    <td style="padding:15px">Add actions to package</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/action/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importPackageExtended(callback)</td>
    <td style="padding:15px">Import package based on a import specification</td>
    <td style="padding:15px">{base_path}/{version}/packages/extendedImport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addActionToPackage(packageName, categoryName, actionName, callback)</td>
    <td style="padding:15px">Add action to package</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/action/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebuildPackage(packageName, callback)</td>
    <td style="padding:15px">Rebuild package</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/rebuild?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflowElement(packageName, id, callback)</td>
    <td style="padding:15px">Delete workflow element</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/workflow/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addWorkflowCategoryToPackage(packageName, categoryId, callback)</td>
    <td style="padding:15px">Add workflow category to package</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/workflow_category/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteActionElement(packageName, id, callback)</td>
    <td style="padding:15px">Delete action element</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/action/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRecourceCategoryToPackage(packageName, categoryName, callback)</td>
    <td style="padding:15px">Add recource category to package</td>
    <td style="padding:15px">{base_path}/{version}/packages/{pathv1}/resource_category/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionRuleByRuleIdForConfig(id, ruleId, callback)</td>
    <td style="padding:15px">Get permission rule</td>
    <td style="padding:15px">{base_path}/{version}/configurations/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePermissionRuleByRuleIdForConfig(id, ruleId, body, callback)</td>
    <td style="padding:15px">Update permission rule</td>
    <td style="padding:15px">{base_path}/{version}/configurations/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionRuleByRuleIdForConfig(id, ruleId, callback)</td>
    <td style="padding:15px">Delete permission rule</td>
    <td style="padding:15px">{base_path}/{version}/configurations/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportConfiguration(id, callback)</td>
    <td style="padding:15px">Export configuration</td>
    <td style="padding:15px">{base_path}/{version}/configurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConfiguration(id, body, callback)</td>
    <td style="padding:15px">Update ? configuration</td>
    <td style="padding:15px">{base_path}/{version}/configurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfiguration(id, force, callback)</td>
    <td style="padding:15px">Delete configuration</td>
    <td style="padding:15px">{base_path}/{version}/configurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllConfigurations(callback)</td>
    <td style="padding:15px">List all configurations</td>
    <td style="padding:15px">{base_path}/{version}/configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importConfiguration(categoryId, bodyFormData, callback)</td>
    <td style="padding:15px">Import configuration</td>
    <td style="padding:15px">{base_path}/{version}/configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionsForConfiguration(id, callback)</td>
    <td style="padding:15px">Get configuration permissions</td>
    <td style="padding:15px">{base_path}/{version}/configurations/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertPermissionsForConfiguration(id, body, callback)</td>
    <td style="padding:15px">Insert configuration permissions</td>
    <td style="padding:15px">{base_path}/{version}/configurations/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionsForConfig(id, callback)</td>
    <td style="padding:15px">Delete configuration permissions</td>
    <td style="padding:15px">{base_path}/{version}/configurations/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyTemplate(id, body, callback)</td>
    <td style="padding:15px">Update policy template</td>
    <td style="padding:15px">{base_path}/{version}/policies/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyTemplate(id, callback)</td>
    <td style="padding:15px">Delete policy template</td>
    <td style="padding:15px">{base_path}/{version}/policies/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPoliciesForState(state, callback)</td>
    <td style="padding:15px">Get all policies in state</td>
    <td style="padding:15px">{base_path}/{version}/policies/state/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPolicies(callback)</td>
    <td style="padding:15px">Get all policies</td>
    <td style="padding:15px">{base_path}/{version}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPolicyTemplates(callback)</td>
    <td style="padding:15px">Get all policy templates</td>
    <td style="padding:15px">{base_path}/{version}/policies/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyTemplate(body, callback)</td>
    <td style="padding:15px">Create policy template</td>
    <td style="padding:15px">{base_path}/{version}/policies/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicy(id, callback)</td>
    <td style="padding:15px">Delete policy</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyState(id, callback)</td>
    <td style="padding:15px">Get policy state</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyLogs(id, maxResult, conditions, callback)</td>
    <td style="padding:15px">Get policy logs</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/syslogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionsForAction(id, callback)</td>
    <td style="padding:15px">Retrieves action permissions</td>
    <td style="padding:15px">{base_path}/{version}/actions/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertPermissionsForAction(id, body, callback)</td>
    <td style="padding:15px">Sets permissions for an action with given id</td>
    <td style="padding:15px">{base_path}/{version}/actions/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionsForAction(id, callback)</td>
    <td style="padding:15px">Deletes all permissions for an action with a given id</td>
    <td style="padding:15px">{base_path}/{version}/actions/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeActionById(actionId, expand, body, callback)</td>
    <td style="padding:15px">Runs an action with given id</td>
    <td style="padding:15px">{base_path}/{version}/actions/{pathv1}/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAction(categoryName, actionName, callback)</td>
    <td style="padding:15px">Retrieves the definition of an action by categoryName and actionName</td>
    <td style="padding:15px">{base_path}/{version}/actions/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionRuleByRuleIdForAction(id, ruleId, callback)</td>
    <td style="padding:15px">Retrieves details for a specific permission rule</td>
    <td style="padding:15px">{base_path}/{version}/actions/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePermissionRuleByRuleIdForAction(id, ruleId, body, callback)</td>
    <td style="padding:15px">Updates principal or access rights for a single permission rule with a given id</td>
    <td style="padding:15px">{base_path}/{version}/actions/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionRuleByRuleIdForAction(id, ruleId, callback)</td>
    <td style="padding:15px">Deletes a single permission rule with a given id</td>
    <td style="padding:15px">{base_path}/{version}/actions/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeActionByName(categoryName, actionName, expand, body, callback)</td>
    <td style="padding:15px">Runs an action with given category and name</td>
    <td style="padding:15px">{base_path}/{version}/actions/{pathv1}/{pathv2}/executions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportActionByActionId(actionId, callback)</td>
    <td style="padding:15px">Export an action with given id</td>
    <td style="padding:15px">{base_path}/{version}/actions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteActionByActionId(actionId, force, callback)</td>
    <td style="padding:15px">Delete an action</td>
    <td style="padding:15px">{base_path}/{version}/actions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllActions(callback)</td>
    <td style="padding:15px">Retrieves a list of all actions</td>
    <td style="padding:15px">{base_path}/{version}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importAction(categoryName, overwrite, bodyFormData, callback)</td>
    <td style="padding:15px">Import an action in the given category</td>
    <td style="padding:15px">{base_path}/{version}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionRuleByRuleIdForCategory(id, ruleId, callback)</td>
    <td style="padding:15px">Get permission rule</td>
    <td style="padding:15px">{base_path}/{version}/categories/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePermissionRuleByRuleIdForCategory(id, ruleId, body, callback)</td>
    <td style="padding:15px">Update permission rule</td>
    <td style="padding:15px">{base_path}/{version}/categories/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionRuleByRuleIdForCategory(id, ruleId, callback)</td>
    <td style="padding:15px">Delete permission rule</td>
    <td style="padding:15px">{base_path}/{version}/categories/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionsForCategory(id, callback)</td>
    <td style="padding:15px">Get category permissions</td>
    <td style="padding:15px">{base_path}/{version}/categories/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertPermissionsForCategory(id, body, callback)</td>
    <td style="padding:15px">Insert permissions</td>
    <td style="padding:15px">{base_path}/{version}/categories/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionsForCategory(id, callback)</td>
    <td style="padding:15px">Deletes all permissions</td>
    <td style="padding:15px">{base_path}/{version}/categories/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCategories(categoryType, isRoot, callback)</td>
    <td style="padding:15px">Get all categories</td>
    <td style="padding:15px">{base_path}/{version}/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRootCategory(body, callback)</td>
    <td style="padding:15px">Add root category</td>
    <td style="padding:15px">{base_path}/{version}/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCategory(id, callback)</td>
    <td style="padding:15px">Get category</td>
    <td style="padding:15px">{base_path}/{version}/categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addChildCategory(id, body, callback)</td>
    <td style="padding:15px">Add child category</td>
    <td style="padding:15px">{base_path}/{version}/categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCategory(id, deleteNonEmptyContent, callback)</td>
    <td style="padding:15px">Delete category</td>
    <td style="padding:15px">{base_path}/{version}/categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUserInteractions(callback)</td>
    <td style="padding:15px">Get all user interactions</td>
    <td style="padding:15px">{base_path}/{version}/interactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerConfiguration(callback)</td>
    <td style="padding:15px">Get server configuration</td>
    <td style="padding:15px">{base_path}/{version}/server-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importServerConfiguration(password, importIdentity, isUpgradeBackup, callback)</td>
    <td style="padding:15px">Import server configuration</td>
    <td style="padding:15px">{base_path}/{version}/server-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScriptingApi(scope, filter, isStrict, callback)</td>
    <td style="padding:15px">Get scripting API</td>
    <td style="padding:15px">{base_path}/{version}/server-configuration/api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getO11nTypes(callback)</td>
    <td style="padding:15px">Get supported types</td>
    <td style="padding:15px">{base_path}/{version}/server-configuration/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCategoryDetails(id, scope, filter, isStrict, callback)</td>
    <td style="padding:15px">Get scripting API for actions grouped by categories</td>
    <td style="padding:15px">{base_path}/{version}/server-configuration/api/category/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginApi(plugin, version, build, scope, filter, isStrict, callback)</td>
    <td style="padding:15px">Get scripting API for a plugin</td>
    <td style="padding:15px">{base_path}/{version}/server-configuration/api/plugins/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSettings(callback)</td>
    <td style="padding:15px">Get subset of the server configuration settings</td>
    <td style="padding:15px">{base_path}/{version}/server-configuration/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportPlugin(pluginName, callback)</td>
    <td style="padding:15px">Export plugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disablePlugin(pluginName, body, callback)</td>
    <td style="padding:15px">Enable/disable plugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllPlugins(callback)</td>
    <td style="padding:15px">List all plugins</td>
    <td style="padding:15px">{base_path}/{version}/plugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importPlugin(format, overwrite, bodyFormData, callback)</td>
    <td style="padding:15px">Import plugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">installPluginDynamically(bodyFormData, callback)</td>
    <td style="padding:15px">Install plugin dynamically</td>
    <td style="padding:15px">{base_path}/{version}/plugins/installPluginDynamically?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendCustomEvent(eventname, param2, callback)</td>
    <td style="padding:15px">Send custom event.</td>
    <td style="padding:15px">{base_path}/{version}/customevent/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionRuleByRuleIdForServer(ruleId, callback)</td>
    <td style="padding:15px">Get permission rule</td>
    <td style="padding:15px">{base_path}/{version}/server/permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePermissionRuleByRuleIdForOrchestratorServer(ruleId, body, callback)</td>
    <td style="padding:15px">Update permission rule</td>
    <td style="padding:15px">{base_path}/{version}/server/permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionRuleByRuleIdForServer(ruleId, callback)</td>
    <td style="padding:15px">Delete permission rule</td>
    <td style="padding:15px">{base_path}/{version}/server/permissions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enumerateServiceByServer(callback)</td>
    <td style="padding:15px">Enumerate services</td>
    <td style="padding:15px">{base_path}/{version}/server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroups(filter, maxResult, callback)</td>
    <td style="padding:15px">Get groups</td>
    <td style="padding:15px">{base_path}/{version}/server/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthentication(callback)</td>
    <td style="padding:15px">Get authentication</td>
    <td style="padding:15px">{base_path}/{version}/server/authentication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionsForOrchestratorServer(callback)</td>
    <td style="padding:15px">Get Orchestrator server permissions</td>
    <td style="padding:15px">{base_path}/{version}/server/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertPermissionsForOrchestratorServer(body, callback)</td>
    <td style="padding:15px">Insert permissions for Orchestrator server</td>
    <td style="padding:15px">{base_path}/{version}/server/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionsForOrchestratorServer(callback)</td>
    <td style="padding:15px">Delete all permissions for the Orchestrator server</td>
    <td style="padding:15px">{base_path}/{version}/server/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stepReturnWorkflow(executionId, callback)</td>
    <td style="padding:15px">StepReturn</td>
    <td style="padding:15px">{base_path}/{version}/debugger/{pathv1}/stepReturn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDebuggerEnabled(callback)</td>
    <td style="padding:15px">Get global debugger state</td>
    <td style="padding:15px">{base_path}/{version}/debugger/enabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDebuggerEnabled(value, callback)</td>
    <td style="padding:15px">Set the global debugger state</td>
    <td style="padding:15px">{base_path}/{version}/debugger/enabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stepOverWorkflow(executionId, callback)</td>
    <td style="padding:15px">StepOver</td>
    <td style="padding:15px">{base_path}/{version}/debugger/{pathv1}/stepOver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowBreakpoints(workflowId, elementName, callback)</td>
    <td style="padding:15px">Get workflow breakpoints</td>
    <td style="padding:15px">{base_path}/{version}/debugger/{pathv1}/breakpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setWorkflowBreakpoints(workflowId, body, callback)</td>
    <td style="padding:15px">Set multiple workflow breakpoints</td>
    <td style="padding:15px">{base_path}/{version}/debugger/{pathv1}/breakpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeAllWorkflowBreakpoints(workflowId, callback)</td>
    <td style="padding:15px">Remove all breakpoints for a workflow</td>
    <td style="padding:15px">{base_path}/{version}/debugger/{pathv1}/breakpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowDebuggerEnabled(executionId, callback)</td>
    <td style="padding:15px">Get debug mode for a specific execution</td>
    <td style="padding:15px">{base_path}/{version}/debugger/{pathv1}/enabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resumeWorkflow(executionId, callback)</td>
    <td style="padding:15px">Resume</td>
    <td style="padding:15px">{base_path}/{version}/debugger/{pathv1}/resume?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeSlotEntry(executionId, body, callback)</td>
    <td style="padding:15px">Set multiple workflow breakpoints</td>
    <td style="padding:15px">{base_path}/{version}/debugger/{pathv1}/newValue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScriptSlotEntries(executionId, callback)</td>
    <td style="padding:15px">Current variables</td>
    <td style="padding:15px">{base_path}/{version}/debugger/{pathv1}/slots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowDebugLocation(executionId, callback)</td>
    <td style="padding:15px">Get workflow debugger location</td>
    <td style="padding:15px">{base_path}/{version}/debugger/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setWorkflowBreakpoint(workflowId, elementName, lineNumber, callback)</td>
    <td style="padding:15px">Set workflow breakpoint</td>
    <td style="padding:15px">{base_path}/{version}/debugger/{pathv1}/breakpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">evalScript(executionId, body, callback)</td>
    <td style="padding:15px">Evaluate script</td>
    <td style="padding:15px">{base_path}/{version}/debugger/{pathv1}/eval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stepIntoWorkflow(executionId, callback)</td>
    <td style="padding:15px">StepInto</td>
    <td style="padding:15px">{base_path}/{version}/debugger/{pathv1}/stepInto?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionRuleByRuleIdForResource(id, ruleId, callback)</td>
    <td style="padding:15px">Get permission rule</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePermissionRuleByRuleIdForResource(id, ruleId, body, callback)</td>
    <td style="padding:15px">Update permission rule</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionRuleByRuleIdForResource(id, ruleId, callback)</td>
    <td style="padding:15px">Delete permission rule</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/permissions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionsForResource(id, callback)</td>
    <td style="padding:15px">Get permissions</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertPermissionsForResource(id, body, callback)</td>
    <td style="padding:15px">Insert permissions</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionsForResource(id, callback)</td>
    <td style="padding:15px">Delete permissions</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllresources(callback)</td>
    <td style="padding:15px">List all resources</td>
    <td style="padding:15px">{base_path}/{version}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importResource(resource, bodyFormData, callback)</td>
    <td style="padding:15px">Import resource</td>
    <td style="padding:15px">{base_path}/{version}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportResource(id, callback)</td>
    <td style="padding:15px">Export resource</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateResource(id, bodyFormData, callback)</td>
    <td style="padding:15px">Update resource content</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResource(id, force, callback)</td>
    <td style="padding:15px">Delete resource</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">browseInventoryPath(maxResult, startIndex, queryCount, keys, conditions, sortOrders, callback)</td>
    <td style="padding:15px">Browse inventory path</td>
    <td style="padding:15px">{base_path}/{version}/inventory/**?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthorizationGroup(body, callback)</td>
    <td style="padding:15px">Creates an Orchestrator's authorization group</td>
    <td style="padding:15px">{base_path}/{version}/authorization-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthorizationGroup(id, callback)</td>
    <td style="padding:15px">Get an Orchestrator's authorization group</td>
    <td style="padding:15px">{base_path}/{version}/authorization-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthorizationGroup(id, callback)</td>
    <td style="padding:15px">Delete an Orchestrator's authorization group</td>
    <td style="padding:15px">{base_path}/{version}/authorization-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addReferenceToGroup(groupId, objectType, objectId, callback)</td>
    <td style="padding:15px">Creates an Orchestrator's authorization group</td>
    <td style="padding:15px">{base_path}/{version}/authorization-groups/{pathv1}/object/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userMeta(callback)</td>
    <td style="padding:15px">Get user information</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
