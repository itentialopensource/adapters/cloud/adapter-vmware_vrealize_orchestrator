/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-vmware_vrealize_orchestrator',
      type: 'VMwarevRealizeOrchestrator',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const VMwarevRealizeOrchestrator = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] VMwarevRealizeOrchestrator Adapter Test', () => {
  describe('VMwarevRealizeOrchestrator Class Tests', () => {
    const a = new VMwarevRealizeOrchestrator(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const taskServiceCreateTaskBodyParam = {
      workflow: {},
      'recurrence-start-date': 'string'
    };
    describe('#createTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTask(taskServiceCreateTaskBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskService', 'createTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskServiceUpdateTaskBodyParam = {
      'recurrence-start-date': 'string',
      'recurrence-end-date': 'string',
      'recurrence-pattern': 'string',
      'start-mode': 'NORMAL',
      name: 'string',
      description: 'string',
      'recurrence-cycle': 'EVERY_WEEKS',
      id: 'string',
      state: 'RUNNING'
    };
    describe('#updateTask - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTask('fakedata', taskServiceUpdateTaskBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response['recurrence-pattern']);
                assert.equal('object', typeof data.response.workflow);
                assert.equal('string', data.response.description);
                assert.equal('EVERY_HOURS', data.response['recurrence-cycle']);
                assert.equal('string', data.response['recurrence-start-date']);
                assert.equal('string', data.response['recurrence-end-date']);
                assert.equal('START_IN_THE_PAST', data.response['start-mode']);
                assert.equal(true, Array.isArray(data.response.parameter));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('SUSPENDED', data.response.state);
                assert.equal('object', typeof data.response.relations);
                assert.equal('string', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskService', 'updateTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskServiceId = 'fakedata';
    const taskServiceInsertPermissionsForTaskBodyParam = {
      permissions: [
        {
          principal: 'string',
          rights: 'string',
          href: 'string',
          relations: {
            total: 2,
            start: 9,
            link: [
              {
                rel: 'string',
                attribute: [
                  {
                    displayValue: 'string',
                    name: 'string',
                    value: 'string'
                  }
                ],
                href: 'string',
                type: 'string'
              }
            ]
          }
        }
      ]
    };
    describe('#insertPermissionsForTask - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insertPermissionsForTask(taskServiceId, taskServiceInsertPermissionsForTaskBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskService', 'insertPermissionsForTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTasks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTasks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskService', 'getTasks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTask - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTask(taskServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response['recurrence-pattern']);
                assert.equal('object', typeof data.response.workflow);
                assert.equal('string', data.response.description);
                assert.equal('EVERY_HOURS', data.response['recurrence-cycle']);
                assert.equal('string', data.response['recurrence-start-date']);
                assert.equal('string', data.response['recurrence-end-date']);
                assert.equal('NORMAL', data.response['start-mode']);
                assert.equal(true, Array.isArray(data.response.parameter));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('RUNNING', data.response.state);
                assert.equal('object', typeof data.response.relations);
                assert.equal('string', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskService', 'getTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExecutions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExecutions(taskServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskService', 'getExecutions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForTask - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionsForTask(taskServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskService', 'getPermissionsForTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskServiceRuleId = 'fakedata';
    const taskServiceUpdatePermissionRuleByRuleIdForTaskBodyParam = {
      principal: 'string',
      rights: 'string',
      href: 'string',
      relations: {
        total: 10,
        start: 9,
        link: [
          {
            rel: 'string',
            attribute: [
              {
                displayValue: 'string',
                name: 'string',
                value: 'string'
              }
            ],
            href: 'string',
            type: 'string'
          }
        ]
      }
    };
    describe('#updatePermissionRuleByRuleIdForTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForTask(taskServiceId, taskServiceRuleId, taskServiceUpdatePermissionRuleByRuleIdForTaskBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskService', 'updatePermissionRuleByRuleIdForTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForTask - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionRuleByRuleIdForTask(taskServiceId, taskServiceRuleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.principal);
                assert.equal('string', data.response.rights);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskService', 'getPermissionRuleByRuleIdForTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let workflowUserInteractionPresentationServiceWorkflowId = 'fakedata';
    let workflowUserInteractionPresentationServiceExecutionId = 'fakedata';
    const workflowUserInteractionPresentationServiceStartInteractionPresentationBodyParam = {
      executionId: 'string',
      profilerOptions: {
        debuggerEnabled: false,
        enabled: false
      },
      parameter: [
        {
          scope: 'LOCAL',
          name: 'string',
          'encrypt-value': false,
          description: 'string',
          type: 'string',
          value: {
            objectType: 'string'
          },
          updated: false
        }
      ]
    };
    describe('#startInteractionPresentation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.startInteractionPresentation(workflowUserInteractionPresentationServiceWorkflowId, workflowUserInteractionPresentationServiceExecutionId, workflowUserInteractionPresentationServiceStartInteractionPresentationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.valid);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response['start-date']);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response['started-by']);
                assert.equal('object', typeof data.response.relations);
                assert.equal('string', data.response['object-id']);
                assert.equal(true, Array.isArray(data.response.steps));
                assert.equal(true, Array.isArray(data.response.parameters));
              } else {
                runCommonAsserts(data, error);
              }
              workflowUserInteractionPresentationServiceWorkflowId = data.response.id;
              workflowUserInteractionPresentationServiceExecutionId = data.response.id;
              saveMockData('WorkflowUserInteractionPresentationService', 'startInteractionPresentation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowUserInteractionPresentationServicePresentationExecutionId = 'fakedata';
    const workflowUserInteractionPresentationServiceUpdatePresentationPostByPresentationExecutionIdBodyParam = {
      executionId: 'string',
      profilerOptions: {
        debuggerEnabled: true,
        enabled: false
      },
      parameter: [
        {
          scope: 'LOCAL',
          name: 'string',
          'encrypt-value': false,
          description: 'string',
          type: 'string',
          value: {
            objectType: 'string'
          },
          updated: true
        }
      ]
    };
    describe('#updatePresentationPostByPresentationExecutionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePresentationPostByPresentationExecutionId(workflowUserInteractionPresentationServiceWorkflowId, workflowUserInteractionPresentationServiceExecutionId, workflowUserInteractionPresentationServicePresentationExecutionId, workflowUserInteractionPresentationServiceUpdatePresentationPostByPresentationExecutionIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.valid);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response['start-date']);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response['started-by']);
                assert.equal('object', typeof data.response.relations);
                assert.equal('string', data.response['object-id']);
                assert.equal(true, Array.isArray(data.response.steps));
                assert.equal(true, Array.isArray(data.response.parameters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowUserInteractionPresentationService', 'updatePresentationPostByPresentationExecutionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPresentationForInteraction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPresentationForInteraction(workflowUserInteractionPresentationServiceWorkflowId, workflowUserInteractionPresentationServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.relations);
                assert.equal(true, Array.isArray(data.response.steps));
                assert.equal(true, Array.isArray(data.response.inputParameters));
                assert.equal(true, Array.isArray(data.response.outputParameters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowUserInteractionPresentationService', 'getPresentationForInteraction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allInteractionPresentations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.allInteractionPresentations(workflowUserInteractionPresentationServiceWorkflowId, workflowUserInteractionPresentationServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowUserInteractionPresentationService', 'allInteractionPresentations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowUserInteractionPresentationServiceUpdatePresentationByPresentationExecutionIdBodyParam = {
      executionId: 'string',
      profilerOptions: {
        debuggerEnabled: true,
        enabled: false
      },
      parameter: [
        {
          scope: 'TOKEN',
          name: 'string',
          'encrypt-value': false,
          description: 'string',
          type: 'string',
          value: {
            objectType: 'string'
          },
          updated: true
        }
      ]
    };
    describe('#updatePresentationByPresentationExecutionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePresentationByPresentationExecutionId(workflowUserInteractionPresentationServiceWorkflowId, workflowUserInteractionPresentationServiceExecutionId, workflowUserInteractionPresentationServicePresentationExecutionId, workflowUserInteractionPresentationServiceUpdatePresentationByPresentationExecutionIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowUserInteractionPresentationService', 'updatePresentationByPresentationExecutionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadExecutionByPresentationExecutionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadExecutionByPresentationExecutionId(workflowUserInteractionPresentationServiceWorkflowId, workflowUserInteractionPresentationServiceExecutionId, workflowUserInteractionPresentationServicePresentationExecutionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.valid);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response['start-date']);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response['started-by']);
                assert.equal('object', typeof data.response.relations);
                assert.equal('string', data.response['object-id']);
                assert.equal(true, Array.isArray(data.response.steps));
                assert.equal(true, Array.isArray(data.response.parameters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowUserInteractionPresentationService', 'loadExecutionByPresentationExecutionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTagOwners - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTagOwners((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(8, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaggingService', 'listTagOwners', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAllTags((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(9, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaggingService', 'listAllTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taggingServiceOwner = 'fakedata';
    describe('#listTagsByOwner - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTagsByOwner(taggingServiceOwner, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(7, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaggingService', 'listTagsByOwner', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taggingServiceTagName = 'fakedata';
    describe('#getTagsByTagNameAndOwner - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTagsByTagNameAndOwner(taggingServiceOwner, taggingServiceTagName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.tag));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaggingService', 'getTagsByTagNameAndOwner', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enumerateServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enumerateServices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.service));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDescriptorService', 'enumerateServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#aboutInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.aboutInfo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response['api-version']);
                assert.equal('string', data.response['build-date']);
                assert.equal('string', data.response['build-number']);
                assert.equal(true, Array.isArray(data.response.feature));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDescriptorService', 'aboutInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#docs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.docs((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDescriptorService', 'docs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceDescriptorService = true;
    describe('#healthStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.healthStatus(true, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response['health-status']);
                assert.equal('string', data.response['instance-id']);
                assert.equal('string', data.response.state);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDescriptorService', 'healthStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getXmlSchema - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getXmlSchema((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
                assert.equal('string', data.response[2]);
                assert.equal('string', data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDescriptorService', 'getXmlSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceDescriptorServiceName = 'fakedata';
    describe('#getXmlSchemaByName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getXmlSchemaByName(serviceDescriptorServiceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDescriptorService', 'getXmlSchemaByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#status - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.status((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.serviceInitializationStatus);
                assert.equal('string', data.response.defaultServiceEndpointType);
                assert.equal('string', data.response.startedTime);
                assert.equal('string', data.response.solutionUser);
                assert.equal('object', typeof data.response.sslCertificateInfo);
                assert.equal('string', data.response.errorMessage);
                assert.equal(false, data.response.initialized);
                assert.equal('string', data.response.serviceName);
                assert.equal('object', typeof data.response.identityCertificateInfo);
                assert.equal('string', data.response.serviceRegistrationId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDescriptorService', 'status', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#supportedApiVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.supportedApiVersions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.version));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDescriptorService', 'supportedApiVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let workflowPresentationServiceWorkflowId = 'fakedata';
    let workflowPresentationServiceExecutionId = 'fakedata';
    const workflowPresentationServiceStartPresentationBodyParam = {
      executionId: 'string',
      profilerOptions: {
        debuggerEnabled: true,
        enabled: false
      },
      parameter: [
        {
          scope: 'TOKEN',
          name: 'string',
          'encrypt-value': false,
          description: 'string',
          type: 'string',
          value: {
            objectType: 'string'
          },
          updated: true
        }
      ]
    };
    describe('#startPresentation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.startPresentation(workflowPresentationServiceWorkflowId, workflowPresentationServiceStartPresentationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.valid);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response['start-date']);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response['started-by']);
                assert.equal('object', typeof data.response.relations);
                assert.equal('string', data.response['object-id']);
                assert.equal(true, Array.isArray(data.response.steps));
                assert.equal(true, Array.isArray(data.response.parameters));
              } else {
                runCommonAsserts(data, error);
              }
              workflowPresentationServiceWorkflowId = data.response.id;
              workflowPresentationServiceExecutionId = data.response.id;
              saveMockData('WorkflowPresentationService', 'startPresentation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowPresentationServiceUpdatePresentationPostByExecutionIdBodyParam = {
      executionId: 'string',
      profilerOptions: {
        debuggerEnabled: false,
        enabled: false
      },
      parameter: [
        {
          scope: 'LOCAL',
          name: 'string',
          'encrypt-value': false,
          description: 'string',
          type: 'string',
          value: {
            objectType: 'string'
          },
          updated: true
        }
      ]
    };
    describe('#updatePresentationPostByExecutionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePresentationPostByExecutionId(workflowPresentationServiceWorkflowId, workflowPresentationServiceExecutionId, workflowPresentationServiceUpdatePresentationPostByExecutionIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.valid);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response['start-date']);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response['started-by']);
                assert.equal('object', typeof data.response.relations);
                assert.equal('string', data.response['object-id']);
                assert.equal(true, Array.isArray(data.response.steps));
                assert.equal(true, Array.isArray(data.response.parameters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowPresentationService', 'updatePresentationPostByExecutionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPresentationFor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPresentationFor(workflowPresentationServiceWorkflowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.relations);
                assert.equal(true, Array.isArray(data.response.steps));
                assert.equal(true, Array.isArray(data.response.inputParameters));
                assert.equal(true, Array.isArray(data.response.outputParameters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowPresentationService', 'getPresentationFor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allPresentation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.allPresentation(workflowPresentationServiceWorkflowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowPresentationService', 'allPresentation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowPresentationServiceUpdatePresentationByExecutionIdBodyParam = {
      executionId: 'string',
      profilerOptions: {
        debuggerEnabled: true,
        enabled: false
      },
      parameter: [
        {
          scope: 'LOCAL',
          name: 'string',
          'encrypt-value': false,
          description: 'string',
          type: 'string',
          value: {
            objectType: 'string'
          },
          updated: true
        }
      ]
    };
    describe('#updatePresentationByExecutionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePresentationByExecutionId(workflowPresentationServiceWorkflowId, workflowPresentationServiceExecutionId, workflowPresentationServiceUpdatePresentationByExecutionIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowPresentationService', 'updatePresentationByExecutionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadExecutionByExecutionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadExecutionByExecutionId(workflowPresentationServiceWorkflowId, workflowPresentationServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.valid);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response['start-date']);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response['started-by']);
                assert.equal('object', typeof data.response.relations);
                assert.equal('string', data.response['object-id']);
                assert.equal(true, Array.isArray(data.response.steps));
                assert.equal(true, Array.isArray(data.response.parameters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowPresentationService', 'loadExecutionByExecutionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowServiceCategoryId = 'fakedata';
    const workflowServiceBodyFormData = 'fakedata';
    describe('#importWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importWorkflow(workflowServiceCategoryId, null, workflowServiceBodyFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'importWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowServiceId = 'fakedata';
    const workflowServiceInsertPermissionsForWorkflowBodyParam = {
      permissions: [
        {
          principal: 'string',
          rights: 'string',
          href: 'string',
          relations: {
            total: 5,
            start: 9,
            link: [
              {
                rel: 'string',
                attribute: [
                  {
                    displayValue: 'string',
                    name: 'string',
                    value: 'string'
                  }
                ],
                href: 'string',
                type: 'string'
              }
            ]
          }
        }
      ]
    };
    describe('#insertPermissionsForWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insertPermissionsForWorkflow(workflowServiceId, workflowServiceInsertPermissionsForWorkflowBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'insertPermissionsForWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowServiceMaxResult = 555;
    const workflowServiceStartIndex = 555;
    const workflowServiceQueryCount = true;
    describe('#getAllWorkflows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllWorkflows(workflowServiceMaxResult, workflowServiceStartIndex, workflowServiceQueryCount, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(6, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'getAllWorkflows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowServiceValidateBodyParam = {
      description: 'string',
      'workflow-item': [
        {
          description: 'string',
          presentation: {},
          'display-name': 'string',
          position: {}
        }
      ],
      'workflow-note': [
        {
          description: 'string'
        }
      ],
      'ref-types': 'string',
      output: {},
      presentation: {},
      input: {},
      'display-name': 'string',
      attrib: [
        {
          description: 'string'
        }
      ],
      position: {}
    };
    describe('#validate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.validate(workflowServiceValidateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'validate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkflow(workflowServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response['customized-icon']);
                assert.equal('object', typeof data.response['schema-workflow']);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.relations);
                assert.equal(true, Array.isArray(data.response.inputParameters));
                assert.equal('string', data.response.version);
                assert.equal(true, Array.isArray(data.response.outputParameters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'getWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadWorkflowSchemaContent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.downloadWorkflowSchemaContent(workflowServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response['api-version']);
                assert.equal('string', data.response['icon-id']);
                assert.equal('string', data.response['object-name']);
                assert.equal('string', data.response.schemaLocation);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response['workflow-item']));
                assert.equal(true, Array.isArray(data.response['workflow-note']));
                assert.equal('string', data.response.version);
                assert.equal('string', data.response['ref-types']);
                assert.equal('string', data.response['allowed-operations']);
                assert.equal('object', typeof data.response.output);
                assert.equal('object', typeof data.response.presentation);
                assert.equal('object', typeof data.response.input);
                assert.equal(8, data.response.restartMode);
                assert.equal(5, data.response.resumeFromFailedMode);
                assert.equal(true, Array.isArray(data.response['error-handler']));
                assert.equal('string', data.response['display-name']);
                assert.equal(true, Array.isArray(data.response.attrib));
                assert.equal('string', data.response['root-name']);
                assert.equal('object', typeof data.response.position);
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'downloadWorkflowSchemaContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadWorkflowIcon - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadWorkflowIcon(workflowServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'downloadWorkflowIcon', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionsForWorkflow(workflowServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'getPermissionsForWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowServiceRuleId = 'fakedata';
    const workflowServiceUpdatePermissionRuleByRuleIdForWorkflowBodyParam = {
      principal: 'string',
      rights: 'string',
      href: 'string',
      relations: {
        total: 7,
        start: 5,
        link: [
          {
            rel: 'string',
            attribute: [
              {
                displayValue: 'string',
                name: 'string',
                value: 'string'
              }
            ],
            href: 'string',
            type: 'string'
          }
        ]
      }
    };
    describe('#updatePermissionRuleByRuleIdForWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForWorkflow(workflowServiceId, workflowServiceRuleId, workflowServiceUpdatePermissionRuleByRuleIdForWorkflowBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'updatePermissionRuleByRuleIdForWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionRuleByRuleIdForWorkflow(workflowServiceId, workflowServiceRuleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.principal);
                assert.equal('string', data.response.rights);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'getPermissionRuleByRuleIdForWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadWorkflowSchema - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadWorkflowSchema(workflowServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'downloadWorkflowSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowServiceWorkflowId = 'fakedata';
    describe('#getAllUserInteractionsForWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllUserInteractionsForWorkflow(workflowServiceWorkflowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(2, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'getAllUserInteractionsForWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTasksForWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllTasksForWorkflow(workflowServiceWorkflowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'getAllTasksForWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateExisting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.validateExisting(workflowServiceWorkflowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.messages));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'validateExisting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const catalogServiceNamespace = 'fakedata';
    const catalogServiceType = 'fakedata';
    const catalogServiceId = 'fakedata';
    const catalogServiceTagObjectBodyParam = {
      name: 'string'
    };
    describe('#tagObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tagObject(catalogServiceNamespace, catalogServiceType, catalogServiceId, catalogServiceTagObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogService', 'tagObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNamespaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listNamespaces((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.attribute));
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogService', 'listNamespaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findRootElement - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findRootElement(catalogServiceNamespace, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.attribute));
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogService', 'findRootElement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchPluginMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.fetchPluginMetadata(catalogServiceNamespace, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.namespace);
                assert.equal(true, Array.isArray(data.response.type));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogService', 'fetchPluginMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadIconForModule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadIconForModule(catalogServiceNamespace, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogService', 'downloadIconForModule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const catalogServiceParentType = 'fakedata';
    const catalogServiceParentId = 'fakedata';
    const catalogServiceRelationName = 'fakedata';
    const catalogServiceMaxResult = 555;
    const catalogServiceStartIndex = 555;
    const catalogServiceQueryCount = true;
    describe('#findByRelation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findByRelation(catalogServiceNamespace, catalogServiceParentType, catalogServiceParentId, catalogServiceRelationName, catalogServiceMaxResult, catalogServiceStartIndex, catalogServiceQueryCount, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(10, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogService', 'findByRelation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findSimpleListQuery - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findSimpleListQuery(catalogServiceNamespace, catalogServiceType, catalogServiceMaxResult, catalogServiceStartIndex, null, catalogServiceQueryCount, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(10, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogService', 'findSimpleListQuery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadIconForType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadIconForType(catalogServiceNamespace, catalogServiceType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogService', 'downloadIconForType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findById(catalogServiceNamespace, catalogServiceType, catalogServiceId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.attribute));
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogService', 'findById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listObjectTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listObjectTags(catalogServiceNamespace, catalogServiceType, catalogServiceId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.tag));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogService', 'listObjectTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contentServiceCategoryName = 'fakedata';
    const contentServiceBodyFormData = 'fakedata';
    describe('#importActionByCategoryName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importActionByCategoryName(contentServiceCategoryName, contentServiceBodyFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContentService', 'importActionByCategoryName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importPackageByContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importPackageByContent(null, null, null, null, contentServiceBodyFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContentService', 'importPackageByContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contentServiceCategoryId = 'fakedata';
    describe('#importWorkflowByCategoryId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importWorkflowByCategoryId(contentServiceCategoryId, contentServiceBodyFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContentService', 'importWorkflowByCategoryId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listContentTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listContentTypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(6, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContentService', 'listContentTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contentServiceMaxResult = 555;
    const contentServiceStartIndex = 555;
    const contentServiceQueryCount = true;
    describe('#listActions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listActions(contentServiceMaxResult, contentServiceStartIndex, contentServiceQueryCount, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(10, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContentService', 'listActions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contentServiceActionName = 'fakedata';
    describe('#exportActionByActionName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportActionByActionName(contentServiceActionName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContentService', 'exportActionByActionName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllPackagesByContent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAllPackagesByContent((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(8, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContentService', 'listAllPackagesByContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contentServicePackageName = 'fakedata';
    describe('#exportPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportPackage(contentServicePackageName, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContentService', 'exportPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listWorkflows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listWorkflows(contentServiceMaxResult, contentServiceStartIndex, contentServiceQueryCount, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(6, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContentService', 'listWorkflows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contentServiceWorkflowId = 'fakedata';
    describe('#exportWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportWorkflow(contentServiceWorkflowId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContentService', 'exportWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowRunServiceWorkflowId = 'fakedata';
    const workflowRunServiceStartWorkflowExecutionBodyParam = {
      executionId: 'string',
      profilerOptions: {
        debuggerEnabled: false,
        enabled: true
      },
      parameter: [
        {
          scope: 'TOKEN',
          name: 'string',
          'encrypt-value': true,
          description: 'string',
          type: 'string',
          value: {
            objectType: 'string'
          },
          updated: true
        }
      ]
    };
    describe('#startWorkflowExecution - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.startWorkflowExecution(workflowRunServiceWorkflowId, workflowRunServiceStartWorkflowExecutionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowRunService', 'startWorkflowExecution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowRunServiceExecutionId = 'fakedata';
    const workflowRunServiceAnswerUserInteractionBodyParam = {
      executionId: 'string',
      profilerOptions: {
        debuggerEnabled: true,
        enabled: true
      },
      parameter: [
        {
          scope: 'TOKEN',
          name: 'string',
          'encrypt-value': false,
          description: 'string',
          type: 'string',
          value: {
            objectType: 'string'
          },
          updated: true
        }
      ]
    };
    describe('#answerUserInteraction - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.answerUserInteraction(workflowRunServiceWorkflowId, workflowRunServiceExecutionId, workflowRunServiceAnswerUserInteractionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowRunService', 'answerUserInteraction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowRunServiceMaxResult = 555;
    const workflowRunServiceStartIndex = 555;
    describe('#getAllExecutions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllExecutions(workflowRunServiceWorkflowId, workflowRunServiceMaxResult, workflowRunServiceStartIndex, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowRunService', 'getAllExecutions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowExecution - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getWorkflowExecution(workflowRunServiceWorkflowId, workflowRunServiceExecutionId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowRunService', 'getWorkflowExecution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserInteraction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserInteraction(workflowRunServiceWorkflowId, workflowRunServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.parameter));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.href);
                assert.equal(true, Array.isArray(data.response.assignee));
                assert.equal('FINISHED', data.response.state);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowRunService', 'getUserInteraction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowExecutionLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkflowExecutionLogs(workflowRunServiceWorkflowId, workflowRunServiceExecutionId, workflowRunServiceMaxResult, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.logs));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowRunService', 'getWorkflowExecutionLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowExecutionState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkflowExecutionState(workflowRunServiceWorkflowId, workflowRunServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('STATE_WAITING_ON_SIGNAL', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowRunService', 'getWorkflowExecutionState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowExecutionStatistics - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getWorkflowExecutionStatistics(workflowRunServiceWorkflowId, workflowRunServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowRunService', 'getWorkflowExecutionStatistics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowExecutionSyslogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkflowExecutionSyslogs(workflowRunServiceWorkflowId, workflowRunServiceExecutionId, workflowRunServiceMaxResult, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.logs));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowRunService', 'getWorkflowExecutionSyslogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packagesServiceBodyFormData = 'fakedata';
    describe('#importPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importPackage(null, null, null, null, packagesServiceBodyFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'importPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importPackageExtended - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importPackageExtended((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'importPackageExtended', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let packagesServicePackageName = 'fakedata';
    describe('#getImportPackageDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImportPackageDetails(packagesServiceBodyFormData, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.certificateTrusted);
                assert.equal(true, data.response.packageAlreadyExists);
                assert.equal(true, data.response.certificateValid);
                assert.equal(true, data.response.certificateUnknown);
                assert.equal(false, data.response.contentVerified);
                assert.equal('string', data.response.packageName);
                assert.equal('object', typeof data.response.certificateInfo);
                assert.equal(true, Array.isArray(data.response.importElementDetails));
              } else {
                runCommonAsserts(data, error);
              }
              packagesServicePackageName = data.response.packageName;
              saveMockData('PackagesService', 'getImportPackageDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packagesServiceCategoryName = 'fakedata';
    describe('#addActionsToPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addActionsToPackage(packagesServicePackageName, packagesServiceCategoryName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'addActionsToPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packagesServiceActionName = 'fakedata';
    describe('#addActionToPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addActionToPackage(packagesServicePackageName, packagesServiceCategoryName, packagesServiceActionName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'addActionToPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packagesServiceConfigurationId = 'fakedata';
    describe('#addConfigurationElementToPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addConfigurationElementToPackage(packagesServicePackageName, packagesServiceConfigurationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'addConfigurationElementToPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packagesServiceConfigurationCategoryName = 'fakedata';
    describe('#addConfigurationElementCategoryToPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addConfigurationElementCategoryToPackage(packagesServicePackageName, packagesServiceConfigurationCategoryName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'addConfigurationElementCategoryToPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packagesServiceInsertPermissionsForPackageBodyParam = {
      permissions: [
        {
          principal: 'string',
          rights: 'string',
          href: 'string',
          relations: {
            total: 5,
            start: 10,
            link: [
              {
                rel: 'string',
                attribute: [
                  {
                    displayValue: 'string',
                    name: 'string',
                    value: 'string'
                  }
                ],
                href: 'string',
                type: 'string'
              }
            ]
          }
        }
      ]
    };
    describe('#insertPermissionsForPackage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insertPermissionsForPackage(packagesServicePackageName, packagesServiceInsertPermissionsForPackageBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'insertPermissionsForPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rebuildPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rebuildPackage(packagesServicePackageName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'rebuildPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packagesServiceResourceId = 'fakedata';
    describe('#addRecourceElementToPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addRecourceElementToPackage(packagesServicePackageName, packagesServiceResourceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'addRecourceElementToPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRecourceCategoryToPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addRecourceCategoryToPackage(packagesServicePackageName, packagesServiceCategoryName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'addRecourceCategoryToPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packagesServiceWorkflowId = 'fakedata';
    describe('#addWorkflowToPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addWorkflowToPackage(packagesServicePackageName, packagesServiceWorkflowId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'addWorkflowToPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packagesServiceCategoryId = 'fakedata';
    describe('#addWorkflowCategoryToPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addWorkflowCategoryToPackage(packagesServicePackageName, packagesServiceCategoryId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'addWorkflowCategoryToPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllPackages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAllPackages((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(5, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'listAllPackages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packagesServiceCreatePackageBodyParam = {
      description: 'string',
      items: {
        configurations: [
          'string'
        ],
        resources: [
          'string'
        ],
        workflows: [
          'string'
        ],
        'policy-templates': [
          'string'
        ],
        actions: [
          'string'
        ]
      }
    };
    describe('#createPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPackage(packagesServicePackageName, packagesServiceCreatePackageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'createPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packagesServiceUpdatePackageBodyParam = {
      name: 'string',
      description: 'string',
      items: {
        configurations: [
          'string'
        ],
        resources: [
          'string'
        ],
        workflows: [
          'string'
        ],
        'policy-templates': [
          'string'
        ],
        actions: [
          'string'
        ]
      },
      'add-items': {
        configurations: [
          'string'
        ],
        resources: [
          'string'
        ],
        workflows: [
          'string'
        ],
        'policy-templates': [
          'string'
        ],
        actions: [
          'string'
        ]
      },
      'delete-items': {
        configurations: [
          'string'
        ],
        resources: [
          'string'
        ],
        workflows: [
          'string'
        ],
        'policy-templates': [
          'string'
        ],
        actions: [
          'string'
        ]
      }
    };
    describe('#updatePackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePackage(packagesServicePackageName, packagesServiceUpdatePackageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'updatePackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let packagesServiceId = 'fakedata';
    describe('#packageDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.packageDetails(packagesServicePackageName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.policyTemplates));
                assert.equal(true, Array.isArray(data.response.usedPlugins));
                assert.equal(true, Array.isArray(data.response.configurations));
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.resources));
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.workflows));
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.href);
                assert.equal(true, Array.isArray(data.response.actions));
              } else {
                runCommonAsserts(data, error);
              }
              packagesServiceId = data.response.id;
              saveMockData('PackagesService', 'packageDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForObjectPackage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionsForObjectPackage(packagesServicePackageName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'getPermissionsForObjectPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packagesServiceRuleId = 'fakedata';
    const packagesServiceUpdatePermissionRuleByRuleIdForPackageBodyParam = {
      principal: 'string',
      rights: 'string',
      href: 'string',
      relations: {
        total: 3,
        start: 2,
        link: [
          {
            rel: 'string',
            attribute: [
              {
                displayValue: 'string',
                name: 'string',
                value: 'string'
              }
            ],
            href: 'string',
            type: 'string'
          }
        ]
      }
    };
    describe('#updatePermissionRuleByRuleIdForPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForPackage(packagesServicePackageName, packagesServiceRuleId, packagesServiceUpdatePermissionRuleByRuleIdForPackageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'updatePermissionRuleByRuleIdForPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForPackage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionRuleByRuleIdForPackage(packagesServicePackageName, packagesServiceRuleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.principal);
                assert.equal('string', data.response.rights);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'getPermissionRuleByRuleIdForPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationServiceCategoryId = 'fakedata';
    const configurationServiceBodyFormData = 'fakedata';
    describe('#importConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importConfiguration(configurationServiceCategoryId, configurationServiceBodyFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationService', 'importConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationServiceId = 'fakedata';
    const configurationServiceInsertPermissionsForConfigurationBodyParam = {
      permissions: [
        {
          principal: 'string',
          rights: 'string',
          href: 'string',
          relations: {
            total: 2,
            start: 5,
            link: [
              {
                rel: 'string',
                attribute: [
                  {
                    displayValue: 'string',
                    name: 'string',
                    value: 'string'
                  }
                ],
                href: 'string',
                type: 'string'
              }
            ]
          }
        }
      ]
    };
    describe('#insertPermissionsForConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insertPermissionsForConfiguration(configurationServiceId, configurationServiceInsertPermissionsForConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationService', 'insertPermissionsForConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAllConfigurations((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(5, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationService', 'listAllConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationServiceUpdateConfigurationBodyParam = {
      name: 'string',
      description: 'string',
      id: 'string',
      href: 'string',
      attribute: [
        {
          scope: 'TOKEN',
          name: 'string',
          'encrypt-value': false,
          description: 'string',
          type: 'string',
          value: {
            objectType: 'string'
          },
          updated: true
        }
      ],
      relations: {
        total: 3,
        start: 7,
        link: [
          {
            rel: 'string',
            attribute: [
              {
                displayValue: 'string',
                name: 'string',
                value: 'string'
              }
            ],
            href: 'string',
            type: 'string'
          }
        ]
      },
      version: 'string'
    };
    describe('#updateConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateConfiguration(configurationServiceId, configurationServiceUpdateConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationService', 'updateConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportConfiguration(configurationServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationService', 'exportConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionsForConfiguration(configurationServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationService', 'getPermissionsForConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationServiceRuleId = 'fakedata';
    const configurationServiceUpdatePermissionRuleByRuleIdForConfigBodyParam = {
      principal: 'string',
      rights: 'string',
      href: 'string',
      relations: {
        total: 3,
        start: 9,
        link: [
          {
            rel: 'string',
            attribute: [
              {
                displayValue: 'string',
                name: 'string',
                value: 'string'
              }
            ],
            href: 'string',
            type: 'string'
          }
        ]
      }
    };
    describe('#updatePermissionRuleByRuleIdForConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForConfig(configurationServiceId, configurationServiceRuleId, configurationServiceUpdatePermissionRuleByRuleIdForConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationService', 'updatePermissionRuleByRuleIdForConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionRuleByRuleIdForConfig(configurationServiceId, configurationServiceRuleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.principal);
                assert.equal('string', data.response.rights);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationService', 'getPermissionRuleByRuleIdForConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let policyServiceId = 'fakedata';
    const policyServiceCreatePolicyTemplateBodyParam = {
      name: 'string'
    };
    describe('#createPolicyTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicyTemplate(policyServiceCreatePolicyTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response['policy-item']));
                assert.equal(true, Array.isArray(data.response.parameter));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.eventHandlers));
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.relations);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              policyServiceId = data.response.id;
              saveMockData('PolicyService', 'createPolicyTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPolicies((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyService', 'getAllPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyServiceState = 'fakedata';
    describe('#getPoliciesForState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPoliciesForState(policyServiceState, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyService', 'getPoliciesForState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPolicyTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPolicyTemplates((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal(7, data.response.start);
                assert.equal(true, Array.isArray(data.response.link));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyService', 'getAllPolicyTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyServiceUpdatePolicyTemplateBodyParam = {
      name: 'string'
    };
    describe('#updatePolicyTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePolicyTemplate(policyServiceId, policyServiceUpdatePolicyTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyService', 'updatePolicyTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyState - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPolicyState(policyServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyService', 'getPolicyState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyServiceMaxResult = 555;
    describe('#getPolicyLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyLogs(policyServiceId, policyServiceMaxResult, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.logs));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyService', 'getPolicyLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionsServiceCategoryName = 'fakedata';
    const actionsServiceBodyFormData = 'fakedata';
    describe('#importAction - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importAction(actionsServiceCategoryName, null, actionsServiceBodyFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionsService', 'importAction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionsServiceActionId = 'fakedata';
    const actionsServiceExecuteActionByIdBodyParam = {
      executionId: 'string',
      profilerOptions: {
        debuggerEnabled: false,
        enabled: false
      },
      parameter: [
        {
          scope: 'TOKEN',
          name: 'string',
          'encrypt-value': true,
          description: 'string',
          type: 'string',
          value: {
            objectType: 'string'
          },
          updated: true
        }
      ]
    };
    describe('#executeActionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.executeActionById(actionsServiceActionId, null, actionsServiceExecuteActionByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('TOKEN', data.response.scope);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response['encrypt-value']);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.value);
                assert.equal(false, data.response.updated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionsService', 'executeActionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionsServiceActionName = 'fakedata';
    const actionsServiceExecuteActionByNameBodyParam = {
      executionId: 'string',
      profilerOptions: {
        debuggerEnabled: false,
        enabled: false
      },
      parameter: [
        {
          scope: 'LOCAL',
          name: 'string',
          'encrypt-value': true,
          description: 'string',
          type: 'string',
          value: {
            objectType: 'string'
          },
          updated: true
        }
      ]
    };
    describe('#executeActionByName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.executeActionByName(actionsServiceCategoryName, actionsServiceActionName, null, actionsServiceExecuteActionByNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('TOKEN', data.response.scope);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response['encrypt-value']);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.value);
                assert.equal(true, data.response.updated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionsService', 'executeActionByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionsServiceId = 'fakedata';
    const actionsServiceInsertPermissionsForActionBodyParam = {
      permissions: [
        {
          principal: 'string',
          rights: 'string',
          href: 'string',
          relations: {
            total: 10,
            start: 10,
            link: [
              {
                rel: 'string',
                attribute: [
                  {
                    displayValue: 'string',
                    name: 'string',
                    value: 'string'
                  }
                ],
                href: 'string',
                type: 'string'
              }
            ]
          }
        }
      ]
    };
    describe('#insertPermissionsForAction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insertPermissionsForAction(actionsServiceId, actionsServiceInsertPermissionsForActionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionsService', 'insertPermissionsForAction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllActions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAllActions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(9, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionsService', 'listAllActions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportActionByActionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportActionByActionId(actionsServiceActionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionsService', 'exportActionByActionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAction(actionsServiceCategoryName, actionsServiceActionName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.fqn);
                assert.equal(true, Array.isArray(data.response.parameter));
                assert.equal('string', data.response.module);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response['output-type']);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.relations);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.script);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionsService', 'getAction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForAction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionsForAction(actionsServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionsService', 'getPermissionsForAction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionsServiceRuleId = 'fakedata';
    const actionsServiceUpdatePermissionRuleByRuleIdForActionBodyParam = {
      principal: 'string',
      rights: 'string',
      href: 'string',
      relations: {
        total: 7,
        start: 2,
        link: [
          {
            rel: 'string',
            attribute: [
              {
                displayValue: 'string',
                name: 'string',
                value: 'string'
              }
            ],
            href: 'string',
            type: 'string'
          }
        ]
      }
    };
    describe('#updatePermissionRuleByRuleIdForAction - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForAction(actionsServiceId, actionsServiceRuleId, actionsServiceUpdatePermissionRuleByRuleIdForActionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionsService', 'updatePermissionRuleByRuleIdForAction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForAction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionRuleByRuleIdForAction(actionsServiceId, actionsServiceRuleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.principal);
                assert.equal('string', data.response.rights);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionsService', 'getPermissionRuleByRuleIdForAction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let categoryServiceId = 'fakedata';
    let categoryServiceRuleId = 'fakedata';
    const categoryServiceAddRootCategoryBodyParam = {
      name: 'string',
      description: 'string',
      type: 'ConfigurationElementCategory'
    };
    describe('#addRootCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addRootCategory(categoryServiceAddRootCategoryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.path);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.relations);
                assert.equal('WorkflowCategory', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              categoryServiceId = data.response.id;
              categoryServiceRuleId = data.response.id;
              saveMockData('CategoryService', 'addRootCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const categoryServiceAddChildCategoryBodyParam = {
      name: 'string',
      description: 'string',
      type: 'ConfigurationElementCategory'
    };
    describe('#addChildCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addChildCategory(categoryServiceId, categoryServiceAddChildCategoryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.path);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.relations);
                assert.equal('ConfigurationElementCategory', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CategoryService', 'addChildCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const categoryServiceInsertPermissionsForCategoryBodyParam = {
      permissions: [
        {
          principal: 'string',
          rights: 'string',
          href: 'string',
          relations: {
            total: 7,
            start: 2,
            link: [
              {
                rel: 'string',
                attribute: [
                  {
                    displayValue: 'string',
                    name: 'string',
                    value: 'string'
                  }
                ],
                href: 'string',
                type: 'string'
              }
            ]
          }
        }
      ]
    };
    describe('#insertPermissionsForCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insertPermissionsForCategory(categoryServiceId, categoryServiceInsertPermissionsForCategoryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CategoryService', 'insertPermissionsForCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCategories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listCategories(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(8, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CategoryService', 'listCategories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCategory(categoryServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.path);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.href);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.relations);
                assert.equal('WorkflowCategory', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CategoryService', 'getCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionsForCategory(categoryServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CategoryService', 'getPermissionsForCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const categoryServiceUpdatePermissionRuleByRuleIdForCategoryBodyParam = {
      principal: 'string',
      rights: 'string',
      href: 'string',
      relations: {
        total: 1,
        start: 7,
        link: [
          {
            rel: 'string',
            attribute: [
              {
                displayValue: 'string',
                name: 'string',
                value: 'string'
              }
            ],
            href: 'string',
            type: 'string'
          }
        ]
      }
    };
    describe('#updatePermissionRuleByRuleIdForCategory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForCategory(categoryServiceId, categoryServiceRuleId, categoryServiceUpdatePermissionRuleByRuleIdForCategoryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CategoryService', 'updatePermissionRuleByRuleIdForCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionRuleByRuleIdForCategory(categoryServiceId, categoryServiceRuleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.principal);
                assert.equal('string', data.response.rights);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CategoryService', 'getPermissionRuleByRuleIdForCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUserInteractions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllUserInteractions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(1, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInteractionService', 'getAllUserInteractions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importServerConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importServerConfiguration(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerConfigurationService', 'importServerConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServerConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServerConfiguration((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response['config-entry']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerConfigurationService', 'getServerConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScriptingApi - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScriptingApi(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system);
                assert.equal(true, Array.isArray(data.response.scriptCategories));
                assert.equal(true, Array.isArray(data.response.plugins));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerConfigurationService', 'getScriptingApi', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCategoryDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCategoryDetails('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.externalUrl);
                assert.equal('string', data.response.icon);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('ALPHA', data.response.state);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.actions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerConfigurationService', 'getCategoryDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serverConfigurationServicePlugin = 'fakedata';
    describe('#getPluginApi - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPluginApi(serverConfigurationServicePlugin, 'fakedata', 555, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.externalUrl);
                assert.equal(true, Array.isArray(data.response.enums));
                assert.equal(true, Array.isArray(data.response.types));
                assert.equal(8, data.response.build);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal('string', data.response.icon);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('ALPHA', data.response.state);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerConfigurationService', 'getPluginApi', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSettings((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerConfigurationService', 'getSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getO11nTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getO11nTypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response['o11n-type']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServerConfigurationService', 'getO11nTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deprecatedFormat = 'fakedata';
    const deprecatedOverwrite = true;
    const deprecatedBodyFormData = 'fakedata';
    describe('#importPlugin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importPlugin(deprecatedFormat, deprecatedOverwrite, deprecatedBodyFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deprecated', 'importPlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#installPluginDynamically - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.installPluginDynamically(deprecatedBodyFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deprecated', 'installPluginDynamically', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllPlugins - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAllPlugins((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total);
                assert.equal(true, Array.isArray(data.response.plugin));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deprecated', 'listAllPlugins', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deprecatedPluginName = 'fakedata';
    describe('#exportPlugin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportPlugin(deprecatedPluginName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deprecated', 'exportPlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deprecatedDisablePluginBodyParam = {
      enabled: true
    };
    describe('#disablePlugin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disablePlugin(deprecatedPluginName, deprecatedDisablePluginBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deprecated', 'disablePlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customEventServiceEventname = 'fakedata';
    describe('#sendCustomEvent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sendCustomEvent(customEventServiceEventname, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomEventService', 'sendCustomEvent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const orchestratorServerInstanceServiceInsertPermissionsForOrchestratorServerBodyParam = {
      permissions: [
        {
          principal: 'string',
          rights: 'string',
          href: 'string',
          relations: {
            total: 5,
            start: 9,
            link: [
              {
                rel: 'string',
                attribute: [
                  {
                    displayValue: 'string',
                    name: 'string',
                    value: 'string'
                  }
                ],
                href: 'string',
                type: 'string'
              }
            ]
          }
        }
      ]
    };
    describe('#insertPermissionsForOrchestratorServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insertPermissionsForOrchestratorServer(orchestratorServerInstanceServiceInsertPermissionsForOrchestratorServerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrchestratorServerInstanceService', 'insertPermissionsForOrchestratorServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enumerateServiceByServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enumerateServiceByServer((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.sslCertificate);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrchestratorServerInstanceService', 'enumerateServiceByServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthentication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuthentication((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.mode);
                assert.equal('string', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrchestratorServerInstanceService', 'getAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const orchestratorServerInstanceServiceMaxResult = 555;
    describe('#getGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroups(null, orchestratorServerInstanceServiceMaxResult, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response['partial-result']);
                assert.equal(true, Array.isArray(data.response.groups));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrchestratorServerInstanceService', 'getGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForOrchestratorServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionsForOrchestratorServer((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrchestratorServerInstanceService', 'getPermissionsForOrchestratorServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const orchestratorServerInstanceServiceRuleId = 'fakedata';
    const orchestratorServerInstanceServiceUpdatePermissionRuleByRuleIdForOrchestratorServerBodyParam = {
      principal: 'string',
      rights: 'string',
      href: 'string',
      relations: {
        total: 10,
        start: 1,
        link: [
          {
            rel: 'string',
            attribute: [
              {
                displayValue: 'string',
                name: 'string',
                value: 'string'
              }
            ],
            href: 'string',
            type: 'string'
          }
        ]
      }
    };
    describe('#updatePermissionRuleByRuleIdForOrchestratorServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForOrchestratorServer(orchestratorServerInstanceServiceRuleId, orchestratorServerInstanceServiceUpdatePermissionRuleByRuleIdForOrchestratorServerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrchestratorServerInstanceService', 'updatePermissionRuleByRuleIdForOrchestratorServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionRuleByRuleIdForServer(orchestratorServerInstanceServiceRuleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.principal);
                assert.equal('string', data.response.rights);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrchestratorServerInstanceService', 'getPermissionRuleByRuleIdForServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const debuggerServiceValue = true;
    describe('#setDebuggerEnabled - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setDebuggerEnabled(debuggerServiceValue, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'setDebuggerEnabled', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const debuggerServiceExecutionId = 'fakedata';
    const debuggerServiceEvalScriptBodyParam = {
      scripts: [
        'string'
      ]
    };
    describe('#evalScript - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.evalScript(debuggerServiceExecutionId, debuggerServiceEvalScriptBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.variables));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'evalScript', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const debuggerServiceChangeSlotEntryBodyParam = {
      name: 'string'
    };
    describe('#changeSlotEntry - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeSlotEntry(debuggerServiceExecutionId, debuggerServiceChangeSlotEntryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.variables));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'changeSlotEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resumeWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resumeWorkflow(debuggerServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'resumeWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stepIntoWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.stepIntoWorkflow(debuggerServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'stepIntoWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stepOverWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.stepOverWorkflow(debuggerServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'stepOverWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stepReturnWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.stepReturnWorkflow(debuggerServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'stepReturnWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setWorkflowBreakpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.setWorkflowBreakpoint('fakedata', 'fakedata', 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.active);
                assert.equal(3, data.response.lineNumber);
                assert.equal('string', data.response.workflowId);
                assert.equal('string', data.response.elementName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'setWorkflowBreakpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const debuggerServiceWorkflowId = 'fakedata';
    const debuggerServiceSetWorkflowBreakpointsBodyParam = {
      breakpoints: [
        {
          active: true,
          lineNumber: 9,
          workflowId: 'string',
          elementName: 'string'
        }
      ]
    };
    describe('#setWorkflowBreakpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.setWorkflowBreakpoints(debuggerServiceWorkflowId, debuggerServiceSetWorkflowBreakpointsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.breakpoints));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'setWorkflowBreakpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDebuggerEnabled - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDebuggerEnabled((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'getDebuggerEnabled', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let debuggerServiceElementName = 'fakedata';
    let debuggerServiceLineNumber = 'fakedata';
    describe('#getWorkflowDebugLocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkflowDebugLocation(debuggerServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.workflowTokenId);
                assert.equal(6, data.response.lineNumber);
                assert.equal('string', data.response.dunesObjectType);
                assert.equal(true, data.response.suspended);
                assert.equal('string', data.response.dunesObjectId);
                assert.equal('string', data.response.elementName);
              } else {
                runCommonAsserts(data, error);
              }
              debuggerServiceElementName = data.response.elementName;
              debuggerServiceLineNumber = data.response.lineNumber;
              saveMockData('DebuggerService', 'getWorkflowDebugLocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowDebuggerEnabled - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getWorkflowDebuggerEnabled(debuggerServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'getWorkflowDebuggerEnabled', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScriptSlotEntries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScriptSlotEntries(debuggerServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.variables));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'getScriptSlotEntries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowBreakpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkflowBreakpoints(debuggerServiceWorkflowId, debuggerServiceElementName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.breakpoints));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'getWorkflowBreakpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceServiceBodyFormData = 'fakedata';
    describe('#importResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importResource(null, resourceServiceBodyFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceService', 'importResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceServiceId = 'fakedata';
    describe('#updateResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateResource(resourceServiceId, resourceServiceBodyFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceService', 'updateResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceServiceInsertPermissionsForResourceBodyParam = {
      permissions: [
        {
          principal: 'string',
          rights: 'string',
          href: 'string',
          relations: {
            total: 2,
            start: 4,
            link: [
              {
                rel: 'string',
                attribute: [
                  {
                    displayValue: 'string',
                    name: 'string',
                    value: 'string'
                  }
                ],
                href: 'string',
                type: 'string'
              }
            ]
          }
        }
      ]
    };
    describe('#insertPermissionsForResource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.insertPermissionsForResource(resourceServiceId, resourceServiceInsertPermissionsForResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceService', 'insertPermissionsForResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllresources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAllresources((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.total);
                assert.equal(true, Array.isArray(data.response.link));
                assert.equal(10, data.response.start);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceService', 'listAllresources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportResource(resourceServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceService', 'exportResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForResource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionsForResource(resourceServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceService', 'getPermissionsForResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourceServiceRuleId = 'fakedata';
    const resourceServiceUpdatePermissionRuleByRuleIdForResourceBodyParam = {
      principal: 'string',
      rights: 'string',
      href: 'string',
      relations: {
        total: 10,
        start: 5,
        link: [
          {
            rel: 'string',
            attribute: [
              {
                displayValue: 'string',
                name: 'string',
                value: 'string'
              }
            ],
            href: 'string',
            type: 'string'
          }
        ]
      }
    };
    describe('#updatePermissionRuleByRuleIdForResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForResource(resourceServiceId, resourceServiceRuleId, resourceServiceUpdatePermissionRuleByRuleIdForResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceService', 'updatePermissionRuleByRuleIdForResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForResource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissionRuleByRuleIdForResource(resourceServiceId, resourceServiceRuleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.principal);
                assert.equal('string', data.response.rights);
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceService', 'getPermissionRuleByRuleIdForResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryServiceMaxResult = 555;
    const inventoryServiceStartIndex = 555;
    const inventoryServiceQueryCount = true;
    describe('#browseInventoryPath - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.browseInventoryPath(inventoryServiceMaxResult, inventoryServiceStartIndex, inventoryServiceQueryCount, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.attribute));
                assert.equal('string', data.response.href);
                assert.equal('object', typeof data.response.relations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventoryService', 'browseInventoryPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let authorizationGroupsServiceId = 'fakedata';
    let authorizationGroupsServiceGroupId = 'fakedata';
    let authorizationGroupsServiceObjectId = 'fakedata';
    const authorizationGroupsServiceCreateAuthorizationGroupBodyParam = {
      protectedResources: [
        {
          id: 'string',
          type: 'string'
        }
      ],
      authorizedEntities: [
        {
          role: 'ADMINISTRTATOR',
          domain: 'string',
          name: 'string',
          id: 'string',
          type: 'USER'
        }
      ],
      description: 'string',
      id: 'string',
      label: 'string'
    };
    describe('#createAuthorizationGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAuthorizationGroup(authorizationGroupsServiceCreateAuthorizationGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.protectedResources));
                assert.equal(true, Array.isArray(data.response.authorizedEntities));
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.label);
              } else {
                runCommonAsserts(data, error);
              }
              authorizationGroupsServiceId = data.response.id;
              authorizationGroupsServiceGroupId = data.response.id;
              authorizationGroupsServiceObjectId = data.response.id;
              saveMockData('AuthorizationGroupsService', 'createAuthorizationGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizationGroupsServiceObjectType = 'fakedata';
    describe('#addReferenceToGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addReferenceToGroup(authorizationGroupsServiceGroupId, authorizationGroupsServiceObjectType, authorizationGroupsServiceObjectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationGroupsService', 'addReferenceToGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthorizationGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuthorizationGroup(authorizationGroupsServiceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.protectedResources));
                assert.equal(true, Array.isArray(data.response.authorizedEntities));
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.label);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationGroupsService', 'getAuthorizationGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userMeta - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userMeta((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response['admin-rights']);
                assert.equal('string', data.response['solution-user']);
                assert.equal(true, Array.isArray(data.response['user-group']));
                assert.equal('string', data.response['solution-user-domain']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserService', 'userMeta', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTask(taskServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskService', 'deleteTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionsForTask(taskServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskService', 'deletePermissionsForTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForTask(taskServiceId, taskServiceRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaskService', 'deletePermissionRuleByRuleIdForTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowExecutionPresentationInstance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWorkflowExecutionPresentationInstance(workflowUserInteractionPresentationServiceWorkflowId, workflowUserInteractionPresentationServiceExecutionId, workflowUserInteractionPresentationServicePresentationExecutionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowUserInteractionPresentationService', 'deleteWorkflowExecutionPresentationInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#untagObjectsByTagNameAndOwner - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.untagObjectsByTagNameAndOwner(taggingServiceOwner, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaggingService', 'untagObjectsByTagNameAndOwner', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowExecutionInstance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWorkflowExecutionInstance(workflowPresentationServiceExecutionId, workflowPresentationServiceWorkflowId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowPresentationService', 'deleteWorkflowExecutionInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionsForWorkflow(workflowServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'deletePermissionsForWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForWorkflow(workflowServiceId, workflowServiceRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'deletePermissionRuleByRuleIdForWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowByWorkflowId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWorkflowByWorkflowId(workflowServiceWorkflowId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowService', 'deleteWorkflowByWorkflowId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const catalogServiceTagname = 'fakedata';
    describe('#untagObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.untagObject(catalogServiceNamespace, catalogServiceType, catalogServiceId, catalogServiceTagname, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogService', 'untagObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteActionByActionName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteActionByActionName(contentServiceActionName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContentService', 'deleteActionByActionName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePackageWithContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePackageWithContent(contentServicePackageName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContentService', 'deletePackageWithContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowByWorkflowIdInContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWorkflowByWorkflowIdInContent(contentServiceWorkflowId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContentService', 'deleteWorkflowByWorkflowIdInContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowExecution - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWorkflowExecution(workflowRunServiceWorkflowId, workflowRunServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowRunService', 'deleteWorkflowExecution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelWorkflowExecution - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelWorkflowExecution(workflowRunServiceWorkflowId, workflowRunServiceExecutionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowRunService', 'cancelWorkflowExecution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePackage(packagesServicePackageName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'deletePackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteActionElement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteActionElement(packagesServicePackageName, packagesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'deleteActionElement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfigurationElement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteConfigurationElement(packagesServicePackageName, packagesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'deleteConfigurationElement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionsForPackage(packagesServicePackageName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'deletePermissionsForPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForPackage(packagesServicePackageName, packagesServiceRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'deletePermissionRuleByRuleIdForPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteResourceElement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteResourceElement(packagesServicePackageName, packagesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'deleteResourceElement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowElement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWorkflowElement(packagesServicePackageName, packagesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PackagesService', 'deleteWorkflowElement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteConfiguration(configurationServiceId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationService', 'deleteConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionsForConfig(configurationServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationService', 'deletePermissionsForConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForConfig(configurationServiceId, configurationServiceRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationService', 'deletePermissionRuleByRuleIdForConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicyTemplate(policyServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyService', 'deletePolicyTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicy(policyServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PolicyService', 'deletePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteActionByActionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteActionByActionId(actionsServiceActionId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionsService', 'deleteActionByActionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForAction - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionsForAction(actionsServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionsService', 'deletePermissionsForAction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForAction - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForAction(actionsServiceId, actionsServiceRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionsService', 'deletePermissionRuleByRuleIdForAction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCategory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCategory(categoryServiceId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CategoryService', 'deleteCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForCategory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionsForCategory(categoryServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CategoryService', 'deletePermissionsForCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForCategory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForCategory(categoryServiceId, categoryServiceRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CategoryService', 'deletePermissionRuleByRuleIdForCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForOrchestratorServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionsForOrchestratorServer((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrchestratorServerInstanceService', 'deletePermissionsForOrchestratorServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForServer(orchestratorServerInstanceServiceRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrchestratorServerInstanceService', 'deletePermissionRuleByRuleIdForServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeAllWorkflowBreakpoints - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeAllWorkflowBreakpoints(debuggerServiceWorkflowId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebuggerService', 'removeAllWorkflowBreakpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteResource(resourceServiceId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceService', 'deleteResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionsForResource(resourceServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceService', 'deletePermissionsForResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForResource(resourceServiceId, resourceServiceRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResourceService', 'deletePermissionRuleByRuleIdForResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthorizationGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAuthorizationGroup(authorizationGroupsServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-vmware_vrealize_orchestrator-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthorizationGroupsService', 'deleteAuthorizationGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
