/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-vmware_vrealize_orchestrator',
      type: 'VMwarevRealizeOrchestrator',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const VMwarevRealizeOrchestrator = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] VMwarevRealizeOrchestrator Adapter Test', () => {
  describe('VMwarevRealizeOrchestrator Class Tests', () => {
    const a = new VMwarevRealizeOrchestrator(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('vmware_vrealize_orchestrator'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('vmware_vrealize_orchestrator'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('VMwarevRealizeOrchestrator', pronghornDotJson.export);
          assert.equal('VMwarevRealizeOrchestrator', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-vmware_vrealize_orchestrator', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('vmware_vrealize_orchestrator'));
          assert.equal('VMwarevRealizeOrchestrator', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-vmware_vrealize_orchestrator', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-vmware_vrealize_orchestrator', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getPermissionsForTask - errors', () => {
      it('should have a getPermissionsForTask function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionsForTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPermissionsForTask(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionsForTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#insertPermissionsForTask - errors', () => {
      it('should have a insertPermissionsForTask function', (done) => {
        try {
          assert.equal(true, typeof a.insertPermissionsForTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.insertPermissionsForTask(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.insertPermissionsForTask('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForTask - errors', () => {
      it('should have a deletePermissionsForTask function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionsForTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePermissionsForTask(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionsForTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTask - errors', () => {
      it('should have a getTask function', (done) => {
        try {
          assert.equal(true, typeof a.getTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTask(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTask - errors', () => {
      it('should have a updateTask function', (done) => {
        try {
          assert.equal(true, typeof a.updateTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateTask(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updateTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTask('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updateTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTask - errors', () => {
      it('should have a deleteTask function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteTask(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTasks - errors', () => {
      it('should have a getTasks function', (done) => {
        try {
          assert.equal(true, typeof a.getTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTask - errors', () => {
      it('should have a createTask function', (done) => {
        try {
          assert.equal(true, typeof a.createTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTask(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-createTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExecutions - errors', () => {
      it('should have a getExecutions function', (done) => {
        try {
          assert.equal(true, typeof a.getExecutions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExecutions(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getExecutions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForTask - errors', () => {
      it('should have a getPermissionRuleByRuleIdForTask function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionRuleByRuleIdForTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPermissionRuleByRuleIdForTask(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.getPermissionRuleByRuleIdForTask('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePermissionRuleByRuleIdForTask - errors', () => {
      it('should have a updatePermissionRuleByRuleIdForTask function', (done) => {
        try {
          assert.equal(true, typeof a.updatePermissionRuleByRuleIdForTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForTask(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForTask('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForTask('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForTask - errors', () => {
      it('should have a deletePermissionRuleByRuleIdForTask function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionRuleByRuleIdForTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForTask(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForTask('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allInteractionPresentations - errors', () => {
      it('should have a allInteractionPresentations function', (done) => {
        try {
          assert.equal(true, typeof a.allInteractionPresentations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.allInteractionPresentations(null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-allInteractionPresentations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.allInteractionPresentations('fakeparam', null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-allInteractionPresentations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startInteractionPresentation - errors', () => {
      it('should have a startInteractionPresentation function', (done) => {
        try {
          assert.equal(true, typeof a.startInteractionPresentation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.startInteractionPresentation(null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-startInteractionPresentation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.startInteractionPresentation('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-startInteractionPresentation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.startInteractionPresentation('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-startInteractionPresentation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPresentationForInteraction - errors', () => {
      it('should have a getPresentationForInteraction function', (done) => {
        try {
          assert.equal(true, typeof a.getPresentationForInteraction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.getPresentationForInteraction(null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPresentationForInteraction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.getPresentationForInteraction('fakeparam', null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPresentationForInteraction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadExecutionByPresentationExecutionId - errors', () => {
      it('should have a loadExecutionByPresentationExecutionId function', (done) => {
        try {
          assert.equal(true, typeof a.loadExecutionByPresentationExecutionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.loadExecutionByPresentationExecutionId(null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-loadExecutionByPresentationExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.loadExecutionByPresentationExecutionId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-loadExecutionByPresentationExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing presentationExecutionId', (done) => {
        try {
          a.loadExecutionByPresentationExecutionId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'presentationExecutionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-loadExecutionByPresentationExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePresentationByPresentationExecutionId - errors', () => {
      it('should have a updatePresentationByPresentationExecutionId function', (done) => {
        try {
          assert.equal(true, typeof a.updatePresentationByPresentationExecutionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.updatePresentationByPresentationExecutionId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationByPresentationExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.updatePresentationByPresentationExecutionId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationByPresentationExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing presentationExecutionId', (done) => {
        try {
          a.updatePresentationByPresentationExecutionId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'presentationExecutionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationByPresentationExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePresentationByPresentationExecutionId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationByPresentationExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePresentationPostByPresentationExecutionId - errors', () => {
      it('should have a updatePresentationPostByPresentationExecutionId function', (done) => {
        try {
          assert.equal(true, typeof a.updatePresentationPostByPresentationExecutionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.updatePresentationPostByPresentationExecutionId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationPostByPresentationExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.updatePresentationPostByPresentationExecutionId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationPostByPresentationExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing presentationExecutionId', (done) => {
        try {
          a.updatePresentationPostByPresentationExecutionId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'presentationExecutionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationPostByPresentationExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePresentationPostByPresentationExecutionId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationPostByPresentationExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowExecutionPresentationInstance - errors', () => {
      it('should have a deleteWorkflowExecutionPresentationInstance function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWorkflowExecutionPresentationInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.deleteWorkflowExecutionPresentationInstance(null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteWorkflowExecutionPresentationInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.deleteWorkflowExecutionPresentationInstance('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteWorkflowExecutionPresentationInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing presentationExecutionId', (done) => {
        try {
          a.deleteWorkflowExecutionPresentationInstance('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'presentationExecutionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteWorkflowExecutionPresentationInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTagOwners - errors', () => {
      it('should have a listTagOwners function', (done) => {
        try {
          assert.equal(true, typeof a.listTagOwners === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTagsByTagNameAndOwner - errors', () => {
      it('should have a getTagsByTagNameAndOwner function', (done) => {
        try {
          assert.equal(true, typeof a.getTagsByTagNameAndOwner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing owner', (done) => {
        try {
          a.getTagsByTagNameAndOwner(null, null, (data, error) => {
            try {
              const displayE = 'owner is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getTagsByTagNameAndOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagName', (done) => {
        try {
          a.getTagsByTagNameAndOwner('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tagName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getTagsByTagNameAndOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTagsByOwner - errors', () => {
      it('should have a listTagsByOwner function', (done) => {
        try {
          assert.equal(true, typeof a.listTagsByOwner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing owner', (done) => {
        try {
          a.listTagsByOwner(null, (data, error) => {
            try {
              const displayE = 'owner is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-listTagsByOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#untagObjectsByTagNameAndOwner - errors', () => {
      it('should have a untagObjectsByTagNameAndOwner function', (done) => {
        try {
          assert.equal(true, typeof a.untagObjectsByTagNameAndOwner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing owner', (done) => {
        try {
          a.untagObjectsByTagNameAndOwner(null, (data, error) => {
            try {
              const displayE = 'owner is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-untagObjectsByTagNameAndOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllTags - errors', () => {
      it('should have a listAllTags function', (done) => {
        try {
          assert.equal(true, typeof a.listAllTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#aboutInfo - errors', () => {
      it('should have a aboutInfo function', (done) => {
        try {
          assert.equal(true, typeof a.aboutInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getXmlSchemaByName - errors', () => {
      it('should have a getXmlSchemaByName function', (done) => {
        try {
          assert.equal(true, typeof a.getXmlSchemaByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getXmlSchemaByName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getXmlSchemaByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enumerateServices - errors', () => {
      it('should have a enumerateServices function', (done) => {
        try {
          assert.equal(true, typeof a.enumerateServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#docs - errors', () => {
      it('should have a docs function', (done) => {
        try {
          assert.equal(true, typeof a.docs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#healthStatus - errors', () => {
      it('should have a healthStatus function', (done) => {
        try {
          assert.equal(true, typeof a.healthStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ', (done) => {
        try {
          a.healthStatus(null, (data, error) => {
            try {
              const displayE = ' is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-healthStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getXmlSchema - errors', () => {
      it('should have a getXmlSchema function', (done) => {
        try {
          assert.equal(true, typeof a.getXmlSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#status - errors', () => {
      it('should have a status function', (done) => {
        try {
          assert.equal(true, typeof a.status === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#supportedApiVersions - errors', () => {
      it('should have a supportedApiVersions function', (done) => {
        try {
          assert.equal(true, typeof a.supportedApiVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadExecutionByExecutionId - errors', () => {
      it('should have a loadExecutionByExecutionId function', (done) => {
        try {
          assert.equal(true, typeof a.loadExecutionByExecutionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.loadExecutionByExecutionId(null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-loadExecutionByExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.loadExecutionByExecutionId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-loadExecutionByExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePresentationByExecutionId - errors', () => {
      it('should have a updatePresentationByExecutionId function', (done) => {
        try {
          assert.equal(true, typeof a.updatePresentationByExecutionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.updatePresentationByExecutionId(null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationByExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.updatePresentationByExecutionId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationByExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePresentationByExecutionId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationByExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePresentationPostByExecutionId - errors', () => {
      it('should have a updatePresentationPostByExecutionId function', (done) => {
        try {
          assert.equal(true, typeof a.updatePresentationPostByExecutionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.updatePresentationPostByExecutionId(null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationPostByExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.updatePresentationPostByExecutionId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationPostByExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePresentationPostByExecutionId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePresentationPostByExecutionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowExecutionInstance - errors', () => {
      it('should have a deleteWorkflowExecutionInstance function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWorkflowExecutionInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.deleteWorkflowExecutionInstance(null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteWorkflowExecutionInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.deleteWorkflowExecutionInstance('fakeparam', null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteWorkflowExecutionInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPresentationFor - errors', () => {
      it('should have a getPresentationFor function', (done) => {
        try {
          assert.equal(true, typeof a.getPresentationFor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.getPresentationFor(null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPresentationFor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allPresentation - errors', () => {
      it('should have a allPresentation function', (done) => {
        try {
          assert.equal(true, typeof a.allPresentation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.allPresentation(null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-allPresentation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startPresentation - errors', () => {
      it('should have a startPresentation function', (done) => {
        try {
          assert.equal(true, typeof a.startPresentation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.startPresentation(null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-startPresentation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.startPresentation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-startPresentation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadWorkflowSchema - errors', () => {
      it('should have a downloadWorkflowSchema function', (done) => {
        try {
          assert.equal(true, typeof a.downloadWorkflowSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.downloadWorkflowSchema(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-downloadWorkflowSchema', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflow - errors', () => {
      it('should have a getWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.getWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWorkflow(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForWorkflow - errors', () => {
      it('should have a getPermissionsForWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionsForWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPermissionsForWorkflow(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionsForWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#insertPermissionsForWorkflow - errors', () => {
      it('should have a insertPermissionsForWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.insertPermissionsForWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.insertPermissionsForWorkflow(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.insertPermissionsForWorkflow('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForWorkflow - errors', () => {
      it('should have a deletePermissionsForWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionsForWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePermissionsForWorkflow(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionsForWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUserInteractionsForWorkflow - errors', () => {
      it('should have a getAllUserInteractionsForWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.getAllUserInteractionsForWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.getAllUserInteractionsForWorkflow(null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getAllUserInteractionsForWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadWorkflowIcon - errors', () => {
      it('should have a downloadWorkflowIcon function', (done) => {
        try {
          assert.equal(true, typeof a.downloadWorkflowIcon === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.downloadWorkflowIcon(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-downloadWorkflowIcon', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validate - errors', () => {
      it('should have a validate function', (done) => {
        try {
          assert.equal(true, typeof a.validate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.validate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-validate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTasksForWorkflow - errors', () => {
      it('should have a getAllTasksForWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTasksForWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.getAllTasksForWorkflow(null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getAllTasksForWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllWorkflows - errors', () => {
      it('should have a getAllWorkflows function', (done) => {
        try {
          assert.equal(true, typeof a.getAllWorkflows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxResult', (done) => {
        try {
          a.getAllWorkflows(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'maxResult is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getAllWorkflows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startIndex', (done) => {
        try {
          a.getAllWorkflows('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getAllWorkflows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryCount', (done) => {
        try {
          a.getAllWorkflows('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'queryCount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getAllWorkflows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importWorkflow - errors', () => {
      it('should have a importWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.importWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categoryId', (done) => {
        try {
          a.importWorkflow(null, null, null, (data, error) => {
            try {
              const displayE = 'categoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.importWorkflow('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateExisting - errors', () => {
      it('should have a validateExisting function', (done) => {
        try {
          assert.equal(true, typeof a.validateExisting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.validateExisting(null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-validateExisting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowByWorkflowId - errors', () => {
      it('should have a deleteWorkflowByWorkflowId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWorkflowByWorkflowId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.deleteWorkflowByWorkflowId(null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteWorkflowByWorkflowId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadWorkflowSchemaContent - errors', () => {
      it('should have a downloadWorkflowSchemaContent function', (done) => {
        try {
          assert.equal(true, typeof a.downloadWorkflowSchemaContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.downloadWorkflowSchemaContent(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-downloadWorkflowSchemaContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForWorkflow - errors', () => {
      it('should have a getPermissionRuleByRuleIdForWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionRuleByRuleIdForWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPermissionRuleByRuleIdForWorkflow(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.getPermissionRuleByRuleIdForWorkflow('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePermissionRuleByRuleIdForWorkflow - errors', () => {
      it('should have a updatePermissionRuleByRuleIdForWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.updatePermissionRuleByRuleIdForWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForWorkflow(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForWorkflow('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForWorkflow('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForWorkflow - errors', () => {
      it('should have a deletePermissionRuleByRuleIdForWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionRuleByRuleIdForWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForWorkflow(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForWorkflow('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findById - errors', () => {
      it('should have a findById function', (done) => {
        try {
          assert.equal(true, typeof a.findById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing namespace', (done) => {
        try {
          a.findById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'namespace is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.findById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.findById('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listObjectTags - errors', () => {
      it('should have a listObjectTags function', (done) => {
        try {
          assert.equal(true, typeof a.listObjectTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing namespace', (done) => {
        try {
          a.listObjectTags(null, null, null, null, (data, error) => {
            try {
              const displayE = 'namespace is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-listObjectTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.listObjectTags('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-listObjectTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.listObjectTags('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-listObjectTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tagObject - errors', () => {
      it('should have a tagObject function', (done) => {
        try {
          assert.equal(true, typeof a.tagObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing namespace', (done) => {
        try {
          a.tagObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'namespace is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-tagObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.tagObject('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-tagObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.tagObject('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-tagObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.tagObject('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-tagObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchPluginMetadata - errors', () => {
      it('should have a fetchPluginMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.fetchPluginMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing namespace', (done) => {
        try {
          a.fetchPluginMetadata(null, null, (data, error) => {
            try {
              const displayE = 'namespace is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-fetchPluginMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findSimpleListQuery - errors', () => {
      it('should have a findSimpleListQuery function', (done) => {
        try {
          assert.equal(true, typeof a.findSimpleListQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing namespace', (done) => {
        try {
          a.findSimpleListQuery(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'namespace is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findSimpleListQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.findSimpleListQuery('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findSimpleListQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxResult', (done) => {
        try {
          a.findSimpleListQuery('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'maxResult is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findSimpleListQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startIndex', (done) => {
        try {
          a.findSimpleListQuery('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findSimpleListQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryCount', (done) => {
        try {
          a.findSimpleListQuery('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'queryCount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findSimpleListQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findRootElement - errors', () => {
      it('should have a findRootElement function', (done) => {
        try {
          assert.equal(true, typeof a.findRootElement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing namespace', (done) => {
        try {
          a.findRootElement(null, null, (data, error) => {
            try {
              const displayE = 'namespace is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findRootElement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findByRelation - errors', () => {
      it('should have a findByRelation function', (done) => {
        try {
          assert.equal(true, typeof a.findByRelation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing namespace', (done) => {
        try {
          a.findByRelation(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'namespace is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findByRelation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentType', (done) => {
        try {
          a.findByRelation('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'parentType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findByRelation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.findByRelation('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findByRelation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing relationName', (done) => {
        try {
          a.findByRelation('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'relationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findByRelation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxResult', (done) => {
        try {
          a.findByRelation('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'maxResult is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findByRelation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startIndex', (done) => {
        try {
          a.findByRelation('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findByRelation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryCount', (done) => {
        try {
          a.findByRelation('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'queryCount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-findByRelation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNamespaces - errors', () => {
      it('should have a listNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadIconForModule - errors', () => {
      it('should have a downloadIconForModule function', (done) => {
        try {
          assert.equal(true, typeof a.downloadIconForModule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing namespace', (done) => {
        try {
          a.downloadIconForModule(null, (data, error) => {
            try {
              const displayE = 'namespace is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-downloadIconForModule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#untagObject - errors', () => {
      it('should have a untagObject function', (done) => {
        try {
          assert.equal(true, typeof a.untagObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing namespace', (done) => {
        try {
          a.untagObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'namespace is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-untagObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.untagObject('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-untagObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.untagObject('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-untagObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagname', (done) => {
        try {
          a.untagObject('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tagname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-untagObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadIconForType - errors', () => {
      it('should have a downloadIconForType function', (done) => {
        try {
          assert.equal(true, typeof a.downloadIconForType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing namespace', (done) => {
        try {
          a.downloadIconForType(null, null, (data, error) => {
            try {
              const displayE = 'namespace is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-downloadIconForType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.downloadIconForType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-downloadIconForType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importActionByCategoryName - errors', () => {
      it('should have a importActionByCategoryName function', (done) => {
        try {
          assert.equal(true, typeof a.importActionByCategoryName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categoryName', (done) => {
        try {
          a.importActionByCategoryName(null, null, (data, error) => {
            try {
              const displayE = 'categoryName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importActionByCategoryName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.importActionByCategoryName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importActionByCategoryName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listActions - errors', () => {
      it('should have a listActions function', (done) => {
        try {
          assert.equal(true, typeof a.listActions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxResult', (done) => {
        try {
          a.listActions(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'maxResult is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-listActions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startIndex', (done) => {
        try {
          a.listActions('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-listActions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryCount', (done) => {
        try {
          a.listActions('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'queryCount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-listActions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listWorkflows - errors', () => {
      it('should have a listWorkflows function', (done) => {
        try {
          assert.equal(true, typeof a.listWorkflows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxResult', (done) => {
        try {
          a.listWorkflows(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'maxResult is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-listWorkflows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startIndex', (done) => {
        try {
          a.listWorkflows('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-listWorkflows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryCount', (done) => {
        try {
          a.listWorkflows('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'queryCount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-listWorkflows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportActionByActionName - errors', () => {
      it('should have a exportActionByActionName function', (done) => {
        try {
          assert.equal(true, typeof a.exportActionByActionName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing actionName', (done) => {
        try {
          a.exportActionByActionName(null, (data, error) => {
            try {
              const displayE = 'actionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-exportActionByActionName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteActionByActionName - errors', () => {
      it('should have a deleteActionByActionName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteActionByActionName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing actionName', (done) => {
        try {
          a.deleteActionByActionName(null, null, (data, error) => {
            try {
              const displayE = 'actionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteActionByActionName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listContentTypes - errors', () => {
      it('should have a listContentTypes function', (done) => {
        try {
          assert.equal(true, typeof a.listContentTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportPackage - errors', () => {
      it('should have a exportPackage function', (done) => {
        try {
          assert.equal(true, typeof a.exportPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.exportPackage(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-exportPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePackageWithContent - errors', () => {
      it('should have a deletePackageWithContent function', (done) => {
        try {
          assert.equal(true, typeof a.deletePackageWithContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.deletePackageWithContent(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePackageWithContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllPackagesByContent - errors', () => {
      it('should have a listAllPackagesByContent function', (done) => {
        try {
          assert.equal(true, typeof a.listAllPackagesByContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importPackageByContent - errors', () => {
      it('should have a importPackageByContent function', (done) => {
        try {
          assert.equal(true, typeof a.importPackageByContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.importPackageByContent('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importPackageByContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importWorkflowByCategoryId - errors', () => {
      it('should have a importWorkflowByCategoryId function', (done) => {
        try {
          assert.equal(true, typeof a.importWorkflowByCategoryId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categoryId', (done) => {
        try {
          a.importWorkflowByCategoryId(null, null, (data, error) => {
            try {
              const displayE = 'categoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importWorkflowByCategoryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.importWorkflowByCategoryId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importWorkflowByCategoryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportWorkflow - errors', () => {
      it('should have a exportWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.exportWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.exportWorkflow(null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-exportWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowByWorkflowIdInContent - errors', () => {
      it('should have a deleteWorkflowByWorkflowIdInContent function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWorkflowByWorkflowIdInContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.deleteWorkflowByWorkflowIdInContent(null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteWorkflowByWorkflowIdInContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowExecutionState - errors', () => {
      it('should have a getWorkflowExecutionState function', (done) => {
        try {
          assert.equal(true, typeof a.getWorkflowExecutionState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.getWorkflowExecutionState(null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowExecutionState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.getWorkflowExecutionState('fakeparam', null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowExecutionState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelWorkflowExecution - errors', () => {
      it('should have a cancelWorkflowExecution function', (done) => {
        try {
          assert.equal(true, typeof a.cancelWorkflowExecution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.cancelWorkflowExecution(null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-cancelWorkflowExecution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.cancelWorkflowExecution('fakeparam', null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-cancelWorkflowExecution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllExecutions - errors', () => {
      it('should have a getAllExecutions function', (done) => {
        try {
          assert.equal(true, typeof a.getAllExecutions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.getAllExecutions(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getAllExecutions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxResult', (done) => {
        try {
          a.getAllExecutions('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'maxResult is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getAllExecutions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startIndex', (done) => {
        try {
          a.getAllExecutions('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'startIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getAllExecutions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startWorkflowExecution - errors', () => {
      it('should have a startWorkflowExecution function', (done) => {
        try {
          assert.equal(true, typeof a.startWorkflowExecution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.startWorkflowExecution(null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-startWorkflowExecution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.startWorkflowExecution('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-startWorkflowExecution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserInteraction - errors', () => {
      it('should have a getUserInteraction function', (done) => {
        try {
          assert.equal(true, typeof a.getUserInteraction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.getUserInteraction(null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getUserInteraction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.getUserInteraction('fakeparam', null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getUserInteraction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#answerUserInteraction - errors', () => {
      it('should have a answerUserInteraction function', (done) => {
        try {
          assert.equal(true, typeof a.answerUserInteraction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.answerUserInteraction(null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-answerUserInteraction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.answerUserInteraction('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-answerUserInteraction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.answerUserInteraction('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-answerUserInteraction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowExecution - errors', () => {
      it('should have a getWorkflowExecution function', (done) => {
        try {
          assert.equal(true, typeof a.getWorkflowExecution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.getWorkflowExecution(null, null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowExecution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.getWorkflowExecution('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowExecution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowExecution - errors', () => {
      it('should have a deleteWorkflowExecution function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWorkflowExecution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.deleteWorkflowExecution(null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteWorkflowExecution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.deleteWorkflowExecution('fakeparam', null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteWorkflowExecution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowExecutionLogs - errors', () => {
      it('should have a getWorkflowExecutionLogs function', (done) => {
        try {
          assert.equal(true, typeof a.getWorkflowExecutionLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.getWorkflowExecutionLogs(null, null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowExecutionLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.getWorkflowExecutionLogs('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowExecutionLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxResult', (done) => {
        try {
          a.getWorkflowExecutionLogs('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'maxResult is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowExecutionLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowExecutionStatistics - errors', () => {
      it('should have a getWorkflowExecutionStatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getWorkflowExecutionStatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.getWorkflowExecutionStatistics(null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowExecutionStatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.getWorkflowExecutionStatistics('fakeparam', null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowExecutionStatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowExecutionSyslogs - errors', () => {
      it('should have a getWorkflowExecutionSyslogs function', (done) => {
        try {
          assert.equal(true, typeof a.getWorkflowExecutionSyslogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.getWorkflowExecutionSyslogs(null, null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowExecutionSyslogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.getWorkflowExecutionSyslogs('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowExecutionSyslogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxResult', (done) => {
        try {
          a.getWorkflowExecutionSyslogs('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'maxResult is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowExecutionSyslogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#packageDetails - errors', () => {
      it('should have a packageDetails function', (done) => {
        try {
          assert.equal(true, typeof a.packageDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.packageDetails(null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-packageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPackage - errors', () => {
      it('should have a createPackage function', (done) => {
        try {
          assert.equal(true, typeof a.createPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.createPackage(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-createPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePackage - errors', () => {
      it('should have a deletePackage function', (done) => {
        try {
          assert.equal(true, typeof a.deletePackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.deletePackage(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePackage - errors', () => {
      it('should have a updatePackage function', (done) => {
        try {
          assert.equal(true, typeof a.updatePackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.updatePackage(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteResourceElement - errors', () => {
      it('should have a deleteResourceElement function', (done) => {
        try {
          assert.equal(true, typeof a.deleteResourceElement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.deleteResourceElement(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteResourceElement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteResourceElement('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteResourceElement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImportPackageDetails - errors', () => {
      it('should have a getImportPackageDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getImportPackageDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.getImportPackageDetails(null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getImportPackageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addConfigurationElementCategoryToPackage - errors', () => {
      it('should have a addConfigurationElementCategoryToPackage function', (done) => {
        try {
          assert.equal(true, typeof a.addConfigurationElementCategoryToPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.addConfigurationElementCategoryToPackage(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addConfigurationElementCategoryToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configurationCategoryName', (done) => {
        try {
          a.addConfigurationElementCategoryToPackage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'configurationCategoryName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addConfigurationElementCategoryToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllPackages - errors', () => {
      it('should have a listAllPackages function', (done) => {
        try {
          assert.equal(true, typeof a.listAllPackages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importPackage - errors', () => {
      it('should have a importPackage function', (done) => {
        try {
          assert.equal(true, typeof a.importPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.importPackage('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForPackage - errors', () => {
      it('should have a getPermissionRuleByRuleIdForPackage function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionRuleByRuleIdForPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.getPermissionRuleByRuleIdForPackage(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.getPermissionRuleByRuleIdForPackage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePermissionRuleByRuleIdForPackage - errors', () => {
      it('should have a updatePermissionRuleByRuleIdForPackage function', (done) => {
        try {
          assert.equal(true, typeof a.updatePermissionRuleByRuleIdForPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForPackage(null, null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForPackage('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForPackage('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForPackage - errors', () => {
      it('should have a deletePermissionRuleByRuleIdForPackage function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionRuleByRuleIdForPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForPackage(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForPackage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfigurationElement - errors', () => {
      it('should have a deleteConfigurationElement function', (done) => {
        try {
          assert.equal(true, typeof a.deleteConfigurationElement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.deleteConfigurationElement(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteConfigurationElement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteConfigurationElement('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteConfigurationElement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRecourceElementToPackage - errors', () => {
      it('should have a addRecourceElementToPackage function', (done) => {
        try {
          assert.equal(true, typeof a.addRecourceElementToPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.addRecourceElementToPackage(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addRecourceElementToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceId', (done) => {
        try {
          a.addRecourceElementToPackage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'resourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addRecourceElementToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addWorkflowToPackage - errors', () => {
      it('should have a addWorkflowToPackage function', (done) => {
        try {
          assert.equal(true, typeof a.addWorkflowToPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.addWorkflowToPackage(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addWorkflowToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.addWorkflowToPackage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addWorkflowToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addConfigurationElementToPackage - errors', () => {
      it('should have a addConfigurationElementToPackage function', (done) => {
        try {
          assert.equal(true, typeof a.addConfigurationElementToPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.addConfigurationElementToPackage(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addConfigurationElementToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configurationId', (done) => {
        try {
          a.addConfigurationElementToPackage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'configurationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addConfigurationElementToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForObjectPackage - errors', () => {
      it('should have a getPermissionsForObjectPackage function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionsForObjectPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.getPermissionsForObjectPackage(null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionsForObjectPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#insertPermissionsForPackage - errors', () => {
      it('should have a insertPermissionsForPackage function', (done) => {
        try {
          assert.equal(true, typeof a.insertPermissionsForPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.insertPermissionsForPackage(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.insertPermissionsForPackage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForPackage - errors', () => {
      it('should have a deletePermissionsForPackage function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionsForPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.deletePermissionsForPackage(null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionsForPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addActionsToPackage - errors', () => {
      it('should have a addActionsToPackage function', (done) => {
        try {
          assert.equal(true, typeof a.addActionsToPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.addActionsToPackage(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addActionsToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categoryName', (done) => {
        try {
          a.addActionsToPackage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'categoryName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addActionsToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importPackageExtended - errors', () => {
      it('should have a importPackageExtended function', (done) => {
        try {
          assert.equal(true, typeof a.importPackageExtended === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addActionToPackage - errors', () => {
      it('should have a addActionToPackage function', (done) => {
        try {
          assert.equal(true, typeof a.addActionToPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.addActionToPackage(null, null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addActionToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categoryName', (done) => {
        try {
          a.addActionToPackage('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'categoryName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addActionToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing actionName', (done) => {
        try {
          a.addActionToPackage('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'actionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addActionToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rebuildPackage - errors', () => {
      it('should have a rebuildPackage function', (done) => {
        try {
          assert.equal(true, typeof a.rebuildPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.rebuildPackage(null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-rebuildPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowElement - errors', () => {
      it('should have a deleteWorkflowElement function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWorkflowElement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.deleteWorkflowElement(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteWorkflowElement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteWorkflowElement('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteWorkflowElement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addWorkflowCategoryToPackage - errors', () => {
      it('should have a addWorkflowCategoryToPackage function', (done) => {
        try {
          assert.equal(true, typeof a.addWorkflowCategoryToPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.addWorkflowCategoryToPackage(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addWorkflowCategoryToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categoryId', (done) => {
        try {
          a.addWorkflowCategoryToPackage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'categoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addWorkflowCategoryToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteActionElement - errors', () => {
      it('should have a deleteActionElement function', (done) => {
        try {
          assert.equal(true, typeof a.deleteActionElement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.deleteActionElement(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteActionElement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteActionElement('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteActionElement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRecourceCategoryToPackage - errors', () => {
      it('should have a addRecourceCategoryToPackage function', (done) => {
        try {
          assert.equal(true, typeof a.addRecourceCategoryToPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing packageName', (done) => {
        try {
          a.addRecourceCategoryToPackage(null, null, (data, error) => {
            try {
              const displayE = 'packageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addRecourceCategoryToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categoryName', (done) => {
        try {
          a.addRecourceCategoryToPackage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'categoryName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addRecourceCategoryToPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForConfig - errors', () => {
      it('should have a getPermissionRuleByRuleIdForConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionRuleByRuleIdForConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPermissionRuleByRuleIdForConfig(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.getPermissionRuleByRuleIdForConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePermissionRuleByRuleIdForConfig - errors', () => {
      it('should have a updatePermissionRuleByRuleIdForConfig function', (done) => {
        try {
          assert.equal(true, typeof a.updatePermissionRuleByRuleIdForConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForConfig(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForConfig('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForConfig('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForConfig - errors', () => {
      it('should have a deletePermissionRuleByRuleIdForConfig function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionRuleByRuleIdForConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForConfig(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportConfiguration - errors', () => {
      it('should have a exportConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.exportConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.exportConfiguration(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-exportConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateConfiguration - errors', () => {
      it('should have a updateConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.updateConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updateConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updateConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfiguration - errors', () => {
      it('should have a deleteConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.deleteConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllConfigurations - errors', () => {
      it('should have a listAllConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.listAllConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importConfiguration - errors', () => {
      it('should have a importConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.importConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categoryId', (done) => {
        try {
          a.importConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'categoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.importConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForConfiguration - errors', () => {
      it('should have a getPermissionsForConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionsForConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPermissionsForConfiguration(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionsForConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#insertPermissionsForConfiguration - errors', () => {
      it('should have a insertPermissionsForConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.insertPermissionsForConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.insertPermissionsForConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.insertPermissionsForConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForConfig - errors', () => {
      it('should have a deletePermissionsForConfig function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionsForConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePermissionsForConfig(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionsForConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicyTemplate - errors', () => {
      it('should have a updatePolicyTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicyTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePolicyTemplate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePolicyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePolicyTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePolicyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyTemplate - errors', () => {
      it('should have a deletePolicyTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicyTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePolicyTemplate(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePolicyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPoliciesForState - errors', () => {
      it('should have a getPoliciesForState function', (done) => {
        try {
          assert.equal(true, typeof a.getPoliciesForState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.getPoliciesForState(null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPoliciesForState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPolicies - errors', () => {
      it('should have a getAllPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPolicyTemplates - errors', () => {
      it('should have a getAllPolicyTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPolicyTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPolicyTemplate - errors', () => {
      it('should have a createPolicyTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createPolicyTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicy - errors', () => {
      it('should have a deletePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePolicy(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyState - errors', () => {
      it('should have a getPolicyState function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPolicyState(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPolicyState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyLogs - errors', () => {
      it('should have a getPolicyLogs function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPolicyLogs(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPolicyLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxResult', (done) => {
        try {
          a.getPolicyLogs('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'maxResult is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPolicyLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForAction - errors', () => {
      it('should have a getPermissionsForAction function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionsForAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPermissionsForAction(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionsForAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#insertPermissionsForAction - errors', () => {
      it('should have a insertPermissionsForAction function', (done) => {
        try {
          assert.equal(true, typeof a.insertPermissionsForAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.insertPermissionsForAction(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.insertPermissionsForAction('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForAction - errors', () => {
      it('should have a deletePermissionsForAction function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionsForAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePermissionsForAction(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionsForAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#executeActionById - errors', () => {
      it('should have a executeActionById function', (done) => {
        try {
          assert.equal(true, typeof a.executeActionById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing actionId', (done) => {
        try {
          a.executeActionById(null, null, null, (data, error) => {
            try {
              const displayE = 'actionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-executeActionById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.executeActionById('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-executeActionById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAction - errors', () => {
      it('should have a getAction function', (done) => {
        try {
          assert.equal(true, typeof a.getAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categoryName', (done) => {
        try {
          a.getAction(null, null, (data, error) => {
            try {
              const displayE = 'categoryName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing actionName', (done) => {
        try {
          a.getAction('fakeparam', null, (data, error) => {
            try {
              const displayE = 'actionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForAction - errors', () => {
      it('should have a getPermissionRuleByRuleIdForAction function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionRuleByRuleIdForAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPermissionRuleByRuleIdForAction(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.getPermissionRuleByRuleIdForAction('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePermissionRuleByRuleIdForAction - errors', () => {
      it('should have a updatePermissionRuleByRuleIdForAction function', (done) => {
        try {
          assert.equal(true, typeof a.updatePermissionRuleByRuleIdForAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForAction(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForAction('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForAction('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForAction - errors', () => {
      it('should have a deletePermissionRuleByRuleIdForAction function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionRuleByRuleIdForAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForAction(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForAction('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#executeActionByName - errors', () => {
      it('should have a executeActionByName function', (done) => {
        try {
          assert.equal(true, typeof a.executeActionByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categoryName', (done) => {
        try {
          a.executeActionByName(null, null, null, null, (data, error) => {
            try {
              const displayE = 'categoryName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-executeActionByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing actionName', (done) => {
        try {
          a.executeActionByName('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'actionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-executeActionByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.executeActionByName('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-executeActionByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportActionByActionId - errors', () => {
      it('should have a exportActionByActionId function', (done) => {
        try {
          assert.equal(true, typeof a.exportActionByActionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing actionId', (done) => {
        try {
          a.exportActionByActionId(null, (data, error) => {
            try {
              const displayE = 'actionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-exportActionByActionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteActionByActionId - errors', () => {
      it('should have a deleteActionByActionId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteActionByActionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing actionId', (done) => {
        try {
          a.deleteActionByActionId(null, null, (data, error) => {
            try {
              const displayE = 'actionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteActionByActionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllActions - errors', () => {
      it('should have a listAllActions function', (done) => {
        try {
          assert.equal(true, typeof a.listAllActions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importAction - errors', () => {
      it('should have a importAction function', (done) => {
        try {
          assert.equal(true, typeof a.importAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing categoryName', (done) => {
        try {
          a.importAction(null, null, null, (data, error) => {
            try {
              const displayE = 'categoryName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.importAction('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForCategory - errors', () => {
      it('should have a getPermissionRuleByRuleIdForCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionRuleByRuleIdForCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPermissionRuleByRuleIdForCategory(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.getPermissionRuleByRuleIdForCategory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePermissionRuleByRuleIdForCategory - errors', () => {
      it('should have a updatePermissionRuleByRuleIdForCategory function', (done) => {
        try {
          assert.equal(true, typeof a.updatePermissionRuleByRuleIdForCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForCategory(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForCategory('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForCategory('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForCategory - errors', () => {
      it('should have a deletePermissionRuleByRuleIdForCategory function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionRuleByRuleIdForCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForCategory(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForCategory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForCategory - errors', () => {
      it('should have a getPermissionsForCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionsForCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPermissionsForCategory(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionsForCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#insertPermissionsForCategory - errors', () => {
      it('should have a insertPermissionsForCategory function', (done) => {
        try {
          assert.equal(true, typeof a.insertPermissionsForCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.insertPermissionsForCategory(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.insertPermissionsForCategory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForCategory - errors', () => {
      it('should have a deletePermissionsForCategory function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionsForCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePermissionsForCategory(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionsForCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCategories - errors', () => {
      it('should have a listCategories function', (done) => {
        try {
          assert.equal(true, typeof a.listCategories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addRootCategory - errors', () => {
      it('should have a addRootCategory function', (done) => {
        try {
          assert.equal(true, typeof a.addRootCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addRootCategory(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addRootCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCategory - errors', () => {
      it('should have a getCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getCategory(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addChildCategory - errors', () => {
      it('should have a addChildCategory function', (done) => {
        try {
          assert.equal(true, typeof a.addChildCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.addChildCategory(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addChildCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addChildCategory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addChildCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCategory - errors', () => {
      it('should have a deleteCategory function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteCategory(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUserInteractions - errors', () => {
      it('should have a getAllUserInteractions function', (done) => {
        try {
          assert.equal(true, typeof a.getAllUserInteractions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServerConfiguration - errors', () => {
      it('should have a getServerConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getServerConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importServerConfiguration - errors', () => {
      it('should have a importServerConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.importServerConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScriptingApi - errors', () => {
      it('should have a getScriptingApi function', (done) => {
        try {
          assert.equal(true, typeof a.getScriptingApi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getO11nTypes - errors', () => {
      it('should have a getO11nTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getO11nTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCategoryDetails - errors', () => {
      it('should have a getCategoryDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getCategoryDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getCategoryDetails(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getCategoryDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginApi - errors', () => {
      it('should have a getPluginApi function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginApi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing plugin', (done) => {
        try {
          a.getPluginApi(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'plugin is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPluginApi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getPluginApi('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPluginApi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing build', (done) => {
        try {
          a.getPluginApi('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'build is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPluginApi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSettings - errors', () => {
      it('should have a getSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportPlugin - errors', () => {
      it('should have a exportPlugin function', (done) => {
        try {
          assert.equal(true, typeof a.exportPlugin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pluginName', (done) => {
        try {
          a.exportPlugin(null, (data, error) => {
            try {
              const displayE = 'pluginName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-exportPlugin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disablePlugin - errors', () => {
      it('should have a disablePlugin function', (done) => {
        try {
          assert.equal(true, typeof a.disablePlugin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pluginName', (done) => {
        try {
          a.disablePlugin(null, null, (data, error) => {
            try {
              const displayE = 'pluginName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-disablePlugin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.disablePlugin('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-disablePlugin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllPlugins - errors', () => {
      it('should have a listAllPlugins function', (done) => {
        try {
          assert.equal(true, typeof a.listAllPlugins === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importPlugin - errors', () => {
      it('should have a importPlugin function', (done) => {
        try {
          assert.equal(true, typeof a.importPlugin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing format', (done) => {
        try {
          a.importPlugin(null, null, null, (data, error) => {
            try {
              const displayE = 'format is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importPlugin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overwrite', (done) => {
        try {
          a.importPlugin('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'overwrite is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importPlugin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.importPlugin('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importPlugin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#installPluginDynamically - errors', () => {
      it('should have a installPluginDynamically function', (done) => {
        try {
          assert.equal(true, typeof a.installPluginDynamically === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.installPluginDynamically(null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-installPluginDynamically', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sendCustomEvent - errors', () => {
      it('should have a sendCustomEvent function', (done) => {
        try {
          assert.equal(true, typeof a.sendCustomEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventname', (done) => {
        try {
          a.sendCustomEvent(null, null, (data, error) => {
            try {
              const displayE = 'eventname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-sendCustomEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForServer - errors', () => {
      it('should have a getPermissionRuleByRuleIdForServer function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionRuleByRuleIdForServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.getPermissionRuleByRuleIdForServer(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePermissionRuleByRuleIdForOrchestratorServer - errors', () => {
      it('should have a updatePermissionRuleByRuleIdForOrchestratorServer function', (done) => {
        try {
          assert.equal(true, typeof a.updatePermissionRuleByRuleIdForOrchestratorServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForOrchestratorServer(null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForOrchestratorServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForOrchestratorServer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForOrchestratorServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForServer - errors', () => {
      it('should have a deletePermissionRuleByRuleIdForServer function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionRuleByRuleIdForServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForServer(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enumerateServiceByServer - errors', () => {
      it('should have a enumerateServiceByServer function', (done) => {
        try {
          assert.equal(true, typeof a.enumerateServiceByServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroups - errors', () => {
      it('should have a getGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxResult', (done) => {
        try {
          a.getGroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'maxResult is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthentication - errors', () => {
      it('should have a getAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForOrchestratorServer - errors', () => {
      it('should have a getPermissionsForOrchestratorServer function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionsForOrchestratorServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#insertPermissionsForOrchestratorServer - errors', () => {
      it('should have a insertPermissionsForOrchestratorServer function', (done) => {
        try {
          assert.equal(true, typeof a.insertPermissionsForOrchestratorServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.insertPermissionsForOrchestratorServer(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForOrchestratorServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForOrchestratorServer - errors', () => {
      it('should have a deletePermissionsForOrchestratorServer function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionsForOrchestratorServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stepReturnWorkflow - errors', () => {
      it('should have a stepReturnWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.stepReturnWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.stepReturnWorkflow(null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-stepReturnWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDebuggerEnabled - errors', () => {
      it('should have a getDebuggerEnabled function', (done) => {
        try {
          assert.equal(true, typeof a.getDebuggerEnabled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setDebuggerEnabled - errors', () => {
      it('should have a setDebuggerEnabled function', (done) => {
        try {
          assert.equal(true, typeof a.setDebuggerEnabled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing value', (done) => {
        try {
          a.setDebuggerEnabled(null, (data, error) => {
            try {
              const displayE = 'value is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-setDebuggerEnabled', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stepOverWorkflow - errors', () => {
      it('should have a stepOverWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.stepOverWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.stepOverWorkflow(null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-stepOverWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowBreakpoints - errors', () => {
      it('should have a getWorkflowBreakpoints function', (done) => {
        try {
          assert.equal(true, typeof a.getWorkflowBreakpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.getWorkflowBreakpoints(null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowBreakpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setWorkflowBreakpoints - errors', () => {
      it('should have a setWorkflowBreakpoints function', (done) => {
        try {
          assert.equal(true, typeof a.setWorkflowBreakpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.setWorkflowBreakpoints(null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-setWorkflowBreakpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setWorkflowBreakpoints('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-setWorkflowBreakpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeAllWorkflowBreakpoints - errors', () => {
      it('should have a removeAllWorkflowBreakpoints function', (done) => {
        try {
          assert.equal(true, typeof a.removeAllWorkflowBreakpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.removeAllWorkflowBreakpoints(null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-removeAllWorkflowBreakpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowDebuggerEnabled - errors', () => {
      it('should have a getWorkflowDebuggerEnabled function', (done) => {
        try {
          assert.equal(true, typeof a.getWorkflowDebuggerEnabled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.getWorkflowDebuggerEnabled(null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowDebuggerEnabled', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resumeWorkflow - errors', () => {
      it('should have a resumeWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.resumeWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.resumeWorkflow(null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-resumeWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeSlotEntry - errors', () => {
      it('should have a changeSlotEntry function', (done) => {
        try {
          assert.equal(true, typeof a.changeSlotEntry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.changeSlotEntry(null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-changeSlotEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeSlotEntry('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-changeSlotEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScriptSlotEntries - errors', () => {
      it('should have a getScriptSlotEntries function', (done) => {
        try {
          assert.equal(true, typeof a.getScriptSlotEntries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.getScriptSlotEntries(null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getScriptSlotEntries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowDebugLocation - errors', () => {
      it('should have a getWorkflowDebugLocation function', (done) => {
        try {
          assert.equal(true, typeof a.getWorkflowDebugLocation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.getWorkflowDebugLocation(null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getWorkflowDebugLocation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setWorkflowBreakpoint - errors', () => {
      it('should have a setWorkflowBreakpoint function', (done) => {
        try {
          assert.equal(true, typeof a.setWorkflowBreakpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowId', (done) => {
        try {
          a.setWorkflowBreakpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'workflowId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-setWorkflowBreakpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing elementName', (done) => {
        try {
          a.setWorkflowBreakpoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'elementName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-setWorkflowBreakpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lineNumber', (done) => {
        try {
          a.setWorkflowBreakpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'lineNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-setWorkflowBreakpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#evalScript - errors', () => {
      it('should have a evalScript function', (done) => {
        try {
          assert.equal(true, typeof a.evalScript === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.evalScript(null, null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-evalScript', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.evalScript('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-evalScript', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stepIntoWorkflow - errors', () => {
      it('should have a stepIntoWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.stepIntoWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing executionId', (done) => {
        try {
          a.stepIntoWorkflow(null, (data, error) => {
            try {
              const displayE = 'executionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-stepIntoWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionRuleByRuleIdForResource - errors', () => {
      it('should have a getPermissionRuleByRuleIdForResource function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionRuleByRuleIdForResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPermissionRuleByRuleIdForResource(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.getPermissionRuleByRuleIdForResource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionRuleByRuleIdForResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePermissionRuleByRuleIdForResource - errors', () => {
      it('should have a updatePermissionRuleByRuleIdForResource function', (done) => {
        try {
          assert.equal(true, typeof a.updatePermissionRuleByRuleIdForResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForResource(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForResource('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePermissionRuleByRuleIdForResource('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updatePermissionRuleByRuleIdForResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionRuleByRuleIdForResource - errors', () => {
      it('should have a deletePermissionRuleByRuleIdForResource function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionRuleByRuleIdForResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForResource(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deletePermissionRuleByRuleIdForResource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionRuleByRuleIdForResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForResource - errors', () => {
      it('should have a getPermissionsForResource function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionsForResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPermissionsForResource(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getPermissionsForResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#insertPermissionsForResource - errors', () => {
      it('should have a insertPermissionsForResource function', (done) => {
        try {
          assert.equal(true, typeof a.insertPermissionsForResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.insertPermissionsForResource(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.insertPermissionsForResource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-insertPermissionsForResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsForResource - errors', () => {
      it('should have a deletePermissionsForResource function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionsForResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePermissionsForResource(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deletePermissionsForResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllresources - errors', () => {
      it('should have a listAllresources function', (done) => {
        try {
          assert.equal(true, typeof a.listAllresources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importResource - errors', () => {
      it('should have a importResource function', (done) => {
        try {
          assert.equal(true, typeof a.importResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.importResource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-importResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportResource - errors', () => {
      it('should have a exportResource function', (done) => {
        try {
          assert.equal(true, typeof a.exportResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.exportResource(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-exportResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateResource - errors', () => {
      it('should have a updateResource function', (done) => {
        try {
          assert.equal(true, typeof a.updateResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateResource(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updateResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.updateResource('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-updateResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteResource - errors', () => {
      it('should have a deleteResource function', (done) => {
        try {
          assert.equal(true, typeof a.deleteResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteResource(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#browseInventoryPath - errors', () => {
      it('should have a browseInventoryPath function', (done) => {
        try {
          assert.equal(true, typeof a.browseInventoryPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxResult', (done) => {
        try {
          a.browseInventoryPath(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'maxResult is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-browseInventoryPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startIndex', (done) => {
        try {
          a.browseInventoryPath('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-browseInventoryPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryCount', (done) => {
        try {
          a.browseInventoryPath('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'queryCount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-browseInventoryPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthorizationGroup - errors', () => {
      it('should have a createAuthorizationGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthorizationGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAuthorizationGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-createAuthorizationGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthorizationGroup - errors', () => {
      it('should have a getAuthorizationGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthorizationGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getAuthorizationGroup(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-getAuthorizationGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthorizationGroup - errors', () => {
      it('should have a deleteAuthorizationGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthorizationGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteAuthorizationGroup(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-deleteAuthorizationGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addReferenceToGroup - errors', () => {
      it('should have a addReferenceToGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addReferenceToGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.addReferenceToGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addReferenceToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectType', (done) => {
        try {
          a.addReferenceToGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objectType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addReferenceToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectId', (done) => {
        try {
          a.addReferenceToGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'objectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vrealize_orchestrator-adapter-addReferenceToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userMeta - errors', () => {
      it('should have a userMeta function', (done) => {
        try {
          assert.equal(true, typeof a.userMeta === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
