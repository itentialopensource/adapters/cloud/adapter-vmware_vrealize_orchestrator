
## 0.4.4 [10-16-2024]

* manual remediation changes

See merge request itentialopensource/adapters/adapter-vmware_vrealize_orchestrator!13

---

## 0.4.3 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-vmware_vrealize_orchestrator!10

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:31PM

See merge request itentialopensource/adapters/adapter-vmware_vrealize_orchestrator!9

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:43PM

See merge request itentialopensource/adapters/adapter-vmware_vrealize_orchestrator!8

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-vmware_vrealize_orchestrator!7

---

## 0.3.3 [03-28-2024]

* Changes made at 2024.03.28_13:17PM

See merge request itentialopensource/adapters/cloud/adapter-vmware_vrealize_orchestrator!6

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:32PM

See merge request itentialopensource/adapters/cloud/adapter-vmware_vrealize_orchestrator!5

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:45AM

See merge request itentialopensource/adapters/cloud/adapter-vmware_vrealize_orchestrator!4

---

## 0.3.0 [12-27-2023]

* Migration from Auto Branch

See merge request itentialopensource/adapters/cloud/adapter-vmware_vrealize_orchestrator!3

---

## 0.2.0 [05-23-2022]

* Minor/adapt 2060: Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-vmware_vrealize_orchestrator!1

---

## 0.1.1 [09-24-2021]

- Initial Commit

See commit 969b6ee

---
