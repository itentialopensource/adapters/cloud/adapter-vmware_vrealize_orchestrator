
## 0.2.0 [05-23-2022]

* Minor/adapt 2060: Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-vmware_vrealize_orchestrator!1

---

## 0.1.1 [09-24-2021]

- Initial Commit

See commit 969b6ee

---
